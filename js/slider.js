
//	catalog section


function checkCompare(arCheck)
{
	if (arCheck == null) return;
	$.each(arCheck, function(){
		if (document.getElementById(this))
		{
			$("#"+this).prop("checked",true);
		}
	});
}
function unCheckCompare(arCheck)
{
	if (arCheck == null) return;
	$.each(arCheck, function(){
		if (document.getElementById(this))
		{
			$("#"+this).prop("checked",false);
		}
	});
}

$(document).ready(function(){ 
	$(".tovar a").prepend("<i></i>");
	$(".slider_info_name a").prepend("<i></i>");
	
	$('.photo_gal a').each(function(){
		$(this).bind('click', function() {
			$.fancybox([
				{href: $(this).data('paintsrc'), title: 'Нарисованная',helpers	: {	thumbs	: {width	: 120,	height	: 120}}}, 
				{href: $(this).data('originalsrc'), title: 'Оригинальная', helpers	: {	thumbs	: {width	: 120,	height	: 120}}}]);
		});
	});
});
function stylerLoad(){
	//$('.select, .courier').trigger('refresh');
	$('.basket .select, .basket .courier').styler({selectVisibleOptions:5});
};
BX.addCustomEvent("onAjaxSuccess", stylerLoad); 
//	catalog filtr
$(document).ready(function(){
	$('.basket .select, .basket .courier').styler({selectVisibleOptions:5});
	if($('#top_filtr')) 
		$('#top_filtr input, #top_filtr select').styler();
	if($('#top_photo')) 
		$('#top_photo select').styler();

});

//	left menu
/*
$(document).ready(function() {
	$('#left_more').click(function() {
		if ( jQuery.browser.msie && parseInt(jQuery.browser.version) == 6) {
			if ($('#left_menu').css("display")=="block") {
				$('#left_menu').css("display", "none");
			} else {
				$('#left_menu').css("display", "block");
			}
		} else {
			$('#left_menu').toggle("slow");
		}
		if ($('#left_more').html()=='<p class="skr"><span>Скрыть</span></p>') {
			$('#left_more').html('<p><span>Показать еще</span></p>');
		} else {
			$('#left_more').html('<p class="skr"><span>Скрыть</span></p>');
		}
	});
});*/
//	left menu1
$(document).ready(function() {
	$('.slide_menu').each(function(){
		var leftmenu = $(this).find('.left_menu');
		var leftmore = $(this).find('.left_more');
		console.log(leftmore.html());
		leftmore.click(function() {
			if ( jQuery.browser.msie && parseInt(jQuery.browser.version) == 6) {
				if (leftmenu.css("display")=="block") {
					leftmenu.css("display", "none");
				} else {
					leftmenu.css("display", "block");
				}
			} else {
				leftmenu.toggle("slow");
			}
			if (leftmore.html()=='<p class="skr"><span>Скрыть</span></p>') {
				leftmore.html('<p><span>Показать еще</span></p>');
			} else {
				leftmore.html('<p class="skr"><span>Скрыть</span></p>');
			}
		});
	});
});

 

//	main text
$(document).ready(function(){
	$(".accordion h2:first").addClass("active");
	$(".accordion div:first").addClass("active");
	$(".accordion p:not(:first)").hide();
	$(".accordion h2").click(function(){
		$(this).next("p").slideToggle("slow");
		$(this).toggleClass("active");
		$(this).parent().toggleClass("active");
		$(this).parent().siblings("div").children("h2").removeClass("active");
		$(this).parent().siblings("div").children("p").slideUp("slow");
		$(this).parent().siblings("div").removeClass("active");
	});
});

$(document).ready(function(){
	$(".accordion1 h2:first").addClass("active");
	$(".accordion1 div:first").addClass("active");
	$(".accordion1 span:not(:first)").hide();
	$(".accordion1 h2").click(function(){
		$(this).next("span").slideToggle("slow");
		$(this).toggleClass("active");
		$(this).parent().toggleClass("active");
		$(this).parent().siblings("div").children("h2").removeClass("active");
		$(this).parent().siblings("div").children("span").slideUp("slow");
		$(this).parent().siblings("div").removeClass("active");
	});
});


//	main slider
$(function(){
	$('#slider').anythingSlider({ 
		autoPlay: false,
		buildStartStop: false,
		appendBackTo: '.anythingControls',
		appendForwardTo: '.anythingControls',
		hashTags: false,
		delay: 4000,  
		animationTime: 600,  
		startPanel: 1,
		backText: '&larr; Назад',
		forwardText: 'Вперёд &rarr;',
	});
});


//	main top slider
$(function() {

	$("#slider .slider_wrapper").each(function(index){
	$div = null;
	$('#thumbs'+index).children().each(function(i) {
		if ( i % 3 == 0) {
			$div = $( '<div />' );
			$div.appendTo( '#thumbs'+index );
		}
		$(this).appendTo( $div );
		$(this).addClass( 'itm'+i );
		$(this).click(function() {
			$('#images'+index).trigger( 'slideTo', [i, 0, true] );
		});
	});
	$('#thumbs img.itm0').addClass( 'selected' );

	$('#images'+index).carouFredSel({
		direction: 'up',
		circular: false,
		infinite: false,
		width: 365,
		height: 365,
		items: 1,
		auto: false,
		prev: '#prev'+index+' .images'+index,
		next: '#next'+index+' .images'+index,
		scroll: {
			fx: 'directscroll',
			onBefore: function() {
				var pos = $(this).triggerHandler( 'currentPosition' );
				$('#thumbs'+index+' img').removeClass( 'selected' );
				$('#thumbs'+index+' img.itm'+pos).addClass( 'selected' );
				
				var page = Math.floor( pos / 3 );
				$('#thumbs'+index).trigger( 'slideToPage', page );
			}
		}
	});

	$('#thumbs'+index).carouFredSel({
		direction: 'up',
		circular: false,
		infinite: false,
		width: 105,
		height: 350,
		items: 1,
		align: false,
		auto: false,
		prev: '#prev'+index+' .thumbs'+index,
		next: '#next'+index+' .thumbs'+index
	});
	});
});


//	fancybox


$(document).ready(function() {

	$("a#example1").fancybox();
	
	$('.callback').fancybox();
	$('.photo_report').fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 120,
				height	: 120
			}
		}
	});
		
	$('.fancybox-media')
	.attr('rel', 'media-gallery')
	.fancybox({
		openEffect : 'none',
		closeEffect : 'none',
		prevEffect : 'none',
		nextEffect : 'none',

		arrows : false,
		helpers : {
			media : {},
			buttons : {}
		}
	});

				
				
});



//	For Android
$(function () { $.smartbanner({title: 'Раскраски по номерам', scale: 'auto', price: 'Бесплатно', button: 'СКАЧАТЬ',  icon: '/bitrix/templates/site/img/android_icons.png', }) })