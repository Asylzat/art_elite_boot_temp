<?
foreach($arResult["ITEMS"] as $cell=>$arElement)
{
	$arResult["ITEMS"][$cell]["ADD_URL"] = str_replace('#ID#', $arElement["ID"], $arResult['ADD_URL_TEMPLATE']);
	foreach($arElement["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $photo)
	{
		$arResult["ITEMS"][$cell]["MORE_PHOTO"][] = array(
			"SMALL" => ImgFileResizeFactory::ResizeImgById($photo, 100, 100, "", ImgFileResizeFactory::CENTER_CROP, true),
			"BIG" => ImgFileResizeFactory::ResizeImgById($photo, 365, 365, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/wm_s.png", ImgFileResizeFactory::CENTER_CROP, true)
		);
	}
	if($arElement["PROPERTIES"]["PRODUCER"]["VALUE"])
	{
		$element = CIBlockElement::GetByID($arElement["PROPERTIES"]["PRODUCER"]["VALUE"])->GetNext();
		$arResult["ITEMS"][$cell]["PROPERTIES"]["PRODUCER"]["VALUE"] = $element["NAME"];
	}
	if($arElement["PROPERTIES"]["SIZE2"]["VALUE"])
	{
		$size = CIBlockElement::GetByID($arElement["PROPERTIES"]["SIZE2"]["VALUE"])->GetNext();
		$arResult["ITEMS"][$cell]["PROPERTIES"]["SIZE"]["VALUE"] = $size["NAME"];
	}
}
?>
