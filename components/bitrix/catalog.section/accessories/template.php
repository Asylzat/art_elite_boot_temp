<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ROWS"])):?>
<div class="main_new">
<p class="head">Багет для вашей картины</p> 
<?foreach($arResult["ROWS"] as $cell=>$rowElements):
	foreach($rowElements as $key=>$arElement):
	?><div<?if($cell>2):?> class="bagets_for_element" style="display:none"<?endif;?>>
		<div><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?print_r($arElement["PREVIEW_PICTURE"]["SRC"]);?>" width="<?print_r($arElement["PREVIEW_PICTURE"]["WIDTH"]);?>" height="<?print_r($arElement["PREVIEW_PICTURE"]["HEIGHT"]);?>" alt="<?=$arElement["NAME"]?>" /></a></div>
		<div>
			<p><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></p>
			<p><?print($arElement["PROPERTIES"]["SIZE2"]["VALUE"]);?> см</p>
			<p class="main_price"><?
			if (is_array($arElement["PRICE_MATRIX"])):
				foreach($arElement["PRICE_MATRIX"]["ROWS"] as $ind => $arRow):
					foreach($arElement["PRICE_MATRIX"]["COLS"] as $typeID => $arType):
						if($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"] < $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"])
							echo '<em><span>'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]).'</span></em> <span>'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
						else
							echo '<span>'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
					endforeach;
				endforeach;
			endif;
			?></p>		
			<p><a class="main_by addToCart" href="<?echo $arElement["ADD_URL"]?>">Купить</a></p>		
		</div>	
	</div><?
endforeach;
	endforeach;?>		
<p class="main_new_all"><a id="showbagets" href="">Смотреть все</a><a id="hidebagets" style="display:none" href="">Скрыть</a></p>
<script>
	$('#showbagets').click(function(){
		$('.bagets_for_element').show();
		$(this).hide();
		$('#hidebagets').show();
		return false;
	});
	$('#hidebagets').click(function(){
		$('.bagets_for_element').hide();
		$(this).hide();
		$('#showbagets').show();
		return false;
	});
</script>
</div>
<?endif;?>