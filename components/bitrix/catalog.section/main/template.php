<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="main_new">
<h2>Новые коллекции</h2>
<p class="main_sale_left"><a href="/catalog/tovary-so-skidkoy/">Товары со скидкой</a></p>
<p class="main_new_right"><a href="/catalog/">Перейти в каталог</a></p>
<p class="clear"></p>
<?foreach($arResult["ROWS"] as $cell=>$rowElements):
	foreach($rowElements as $key=>$arElement):?><div><div><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?if($arElement["PREVIEW_PICTURE"]["SRC"]):?><img src="<?print_r($arElement["PREVIEW_PICTURE"]["SRC"]);?>" width="<?print_r($arElement["PREVIEW_PICTURE"]["WIDTH"]);?>" height="<?print_r($arElement["PREVIEW_PICTURE"]["HEIGHT"]);?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /><?endif?></a></div><div><p class="tovar"><a id="element_<?=$arElement["ID"]?>" title="<?=$arElement["NAME"]?>" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></p><?if($arElement["PROPERTIES"]["SIZE"]["VALUE"]):?><p>Размер: <?print($arElement["PROPERTIES"]["SIZE"]["VALUE"]);?>.</p><?endif?><?if($arElement["PROPERTIES"]["COMPLEXITY"]["VALUE"]):?><p><em class="rating_<?print($arElement["PROPERTIES"]["COMPLEXITY"]["VALUE"]);?>"></em> Сложность:</p><?endif?><?if($arElement["PROPERTIES"]["TYPE"]["VALUE"]):?><p>Тип: <?print($arElement["PROPERTIES"]["TYPE"]["VALUE"]);?></p><?endif?><p><label for="<?=$arElement["ID"]?>"><input type="checkbox" id="<?=$arElement["ID"]?>" onclick="AddToCompare(this);"> <span>Сравнить</span></label></p></div><p class="main_price"><?if (is_array($arElement["PRICE_MATRIX"])):?><?foreach($arElement["PRICE_MATRIX"]["ROWS"] as $ind => $arRow):?><?foreach($arElement["PRICE_MATRIX"]["COLS"] as $typeID => $arType):?><?if($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"] < $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"])
									echo '<em>Цена: <span>'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]).'</span></em> <span>'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
								else
									echo '<em>Цена:</em> <span>'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
								?><?endforeach?><?endforeach?><?endif;?></p><p><a class="main_more" href="<?=$arElement["DETAIL_PAGE_URL"]?>">Подробнее</a><?if($arElement["CAN_BUY"]):?><!--noindex--><a href="<?echo $arElement["ADD_URL"]?>" class="main_by addToCart" rel="nofollow"><?=GetMessage("CATALOG_ADD")?></a><!--/noindex--><?else:?><a  class="main_by_none" rel="nofollow" ><?=GetMessage("CATALOG_NOT_AVAILABLE")?></a><?endif;?></p></div><?	endforeach;endforeach;?>
</div>

 