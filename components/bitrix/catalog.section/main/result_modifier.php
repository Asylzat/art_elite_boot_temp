<?
$rows = array(); 
$rowElements = array();
$count = 0;
/*
foreach($arResult["ITEMS"] as $cell1 => $arElement)
{
		$arElement["BUY_URL"] = htmlspecialchars($arParams["BASKET_URL"]."?".$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=".$arElement["ID"]);
		$arElement["ADD_URL"] = htmlspecialchars($arParams["BASKET_URL"]."?".$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=".$arElement["ID"]."&backurl=".$APPLICATION->GetCurPageParam("", Array("ADD2BASKET", "BUY")));
		$arElement["CAN_BUY"] = "Y";

		$arResult["ITEMS"][$cell1] = $arElement;
}*/
foreach($arResult["ITEMS"] as $cell=>$arElement)
{
	if($arElement["PROPERTIES"]["SIZE2"]["VALUE"])
	{
		$size = CIBlockElement::GetByID($arElement["PROPERTIES"]["SIZE2"]["VALUE"])->GetNext();
		$arResult["ITEMS"][$cell]["PROPERTIES"]["SIZE"]["VALUE"] = $size["NAME"];
	}
	$arResult["ITEMS"][$cell]["ADD_URL"] = str_replace('#ID#', $arElement["ID"], $arResult['ADD_URL_TEMPLATE']);
	$count++;
	$producer = CIBlockElement::GetByID($arElement["PROPERTIES"]["PRODUCER"]["VALUE"])->GetNext();
	$arResult["ITEMS"][$cell]["PROPERTIES"]["PRODUCER"]["VALUE"] = $producer["NAME"];
	if (intVal($arElement["PREVIEW_PICTURE"]["ID"]) > 0)
	{
		$arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = ImgFileResizeFactory::ResizeImgById($arElement["PREVIEW_PICTURE"]["ID"], 120, 120, "", ImgFileResizeFactory::CENTER_CROP, true);
	}
	else
	{
		foreach($arElement["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $photo)
		{
			$arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = ImgFileResizeFactory::ResizeImgById($photo, 120, 120, "", ImgFileResizeFactory::CENTER_CROP, true);
			//$arResult["ITEMS"][$cell]["DETAIL_PICTURE"] = ImgFileResizeFactory::ResizeImgById($photo, 800, 600, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/watermark.png");
			break;
		}
	}
	if ($count == $arParams["LINE_ELEMENT_COUNT"])
	{
		$count = 0;
		$rowElements[] = $arResult["ITEMS"][$cell];
		$rows[] = $rowElements;
		$rowElements = array();
	}
	else
	{
		$rowElements[] = $arResult["ITEMS"][$cell];
	}	
		
}
if ($count > 0)
{
	for($i = $count; $i<$arParams["LINE_ELEMENT_COUNT"]; $i++)
		$rowElements[] = array();
	$rows[] = $rowElements;
}
$arResult["ROWS"] = $rows;

$cp = $this->__component; // объект компонента

if (is_object($cp))
{
	// добавим в arResult компонента два поля - MY_TITLE и IS_OBJECT
	$cp->arResult["LIST_PAGE_URL"] = $arResult["LIST_PAGE_URL"];
	$path = array();
	$rsPath = GetIBlockSectionPath($arResult["IBLOCK_ID"], $arResult["IBLOCK_SECTION_ID"]);
	while($arPath=$rsPath->GetNext())
	{
		$path[] = $arPath;
	}
	$cp->arResult["PATH"] = $path;
	//Добавляем ключи arResult, которые мы добавили в result_modifier.php и которые необходимо сохранить в кеше.
    $cp->SetResultCacheKeys(array("LIST_PAGE_URL","PATH"));
	// сохраним их в копии arResult, с которой работает шаблон (с учетом версии main 10.0 и выше)
}



?>
