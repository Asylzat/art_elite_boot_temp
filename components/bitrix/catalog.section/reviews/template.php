<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="reviews">
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<div>
			<p><span><?=strtolower(FormatDate("d F Y", MakeTimeStamp($arElement["DATE_CREATE"])))?></span></p>
			<p><?=$arElement["~PREVIEW_TEXT"]?></p>
			<p><?=$arElement["NAME"]?></p>
		</div>
<?endforeach;?>
</div>
<div class="clear"></div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><?=$arResult["NAV_STRING"]?><?endif;?>
