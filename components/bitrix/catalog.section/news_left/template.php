<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"]) > 0):?>
<div class="left_type">
	<div class="head news"><p>Новости</p></div>		
	<div class="left_block_type left_news">
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
			<p> <?=FormatDate("d F |", MakeTimeStamp($arElement["DATE_ACTIVE_FROM"]))?> <a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></p>
<?endforeach;?>			
			<p class="all"><a href="/news/">Посмотреть все</a> &rarr;</p>	
	</div>
</div>
<?endif;?>