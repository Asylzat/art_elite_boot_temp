<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
$itemImgPrice = array();
$arWaterMark = Array(
	array(
		"name" => "watermark",
		"position" => "bottomright", // ���������
		"type" => "image",
		"size" => "real",
		"file" => $_SERVER["DOCUMENT_ROOT"].'/upload/resize_cache/wm_s.png', // ���� � ��������
		"fill" => "exact",
	)
);
foreach($arResult["ITEMS"] as $key=>$arItem)
{
	if (intVal($arItem["PREVIEW_PICTURE"]["ID"]) > 0)
	{
		$img = CFile::ResizeImageGet( 
			$arItem["PREVIEW_PICTURE"]["ID"], 
			array(width=>"200", height=>"140"), 
			BX_RESIZE_IMAGE_EXACT, 
			true
		);
		if (!empty($img)) 
			$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = array_change_key_case($img, CASE_UPPER);
		
		$img = CFile::ResizeImageGet( 
			$arItem["PREVIEW_PICTURE"]["ID"], 
			array(width=>"800", height=>"600"), 
			BX_RESIZE_IMAGE_PROPORTIONAL, 
			true,
			$arWaterMark
		);
		if (!empty($img)) 
			$arResult["ITEMS"][$key]["DETAIL_PICTURE"]["PAINT"] = array_change_key_case($img, CASE_UPPER);
	}
	if($arItem["PROPERTIES"]["ORIGINAL"]["VALUE"])
	{
		$item = CIBlockElement::GetByID($arItem["PROPERTIES"]["ORIGINAL"]["VALUE"])->GetNextElement();
		$photo = $item->GetProperty("MORE_PHOTO");
		if (intVal($photo["VALUE"][0]) > 0)
		{
			$img = CFile::ResizeImageGet( 
				$photo["VALUE"][0], 
				array(width=>"800", height=>"600"), 
				BX_RESIZE_IMAGE_PROPORTIONAL, 
				true,
				$arWaterMark
			);
			if (!empty($img)) 
				$arResult["ITEMS"][$key]["DETAIL_PICTURE"]["ORIGINAL"] = array_change_key_case($img, CASE_UPPER);
		}
	}
	//$item = CIBlockElement::GetByID($arItem["ID"])->GetNext();
	//$arResult["ITEMS"][$key]["DETAIL_PAGE_URL"] = $item["DETAIL_PAGE_URL"];
	//$arResult["ITEMS"][$key]["PRICES"]['base']["VALUE"] = number_format($arItem["PRICES"]['base']["VALUE"], 0, '.', ' ');
}
?>