<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="photo_gal">
<?foreach($arResult["ITEMS"] as $cell=>$arItem):?>
		<div><a href="javascript::void(0)" data-paintsrc="<?=$arItem["DETAIL_PICTURE"]["PAINT"]["SRC"]?>" data-originalsrc="<?=$arItem["DETAIL_PICTURE"]["ORIGINAL"]["SRC"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" /></a><span><strong><?=$arItem["NAME"]?></strong>, <?=$arItem["PROPERTIES"]["AUTHOR"]["VALUE"]?></span>
		</div>
<?endforeach;?>
</div>
<div class="clear"></div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><?=$arResult["NAV_STRING"]?><?endif;?>