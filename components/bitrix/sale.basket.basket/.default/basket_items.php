<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<h1>Корзина</h1> 

<div class="basket">
<div class="basket_head">
	<div class="del"><a id="clear_cart" href="javascript:void(0)" title="Очистить корзину"></a></div>
	<div class="name">Наименование</div>
	<div class="amt">Количество</div>
	<div class="price">Цена</div>
</div>
<hr />
<?foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $arItem):?>
<div class="basket_tovar">
	<div class="del"><a id="<?=$arItem["ID"]?>" href="javascript:void(0)" title="Удалить товар"></a></div>
	<div class="name"><img src="<?=$arItem["PICTURE"]["SRC"]?>" width="<?=$arItem["PICTURE"]["WIDTH"]?>" height="<?=$arItem["PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" /> <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a> <span>Артикул: <?=$arItem["ARTICUL"]?></span></div>
	<div class="amt"><a class="minus" href="javascript:void(0)">-</a><input name="QUANTITY_<?=$arItem["ID"] ?>" type="text" value="<?=$arItem["QUANTITY"]?>"><a class="plus" href="javascript:void(0)">+</a></div>
	<div class="price">
		<?if ($arItem["DISCOUNT_PRICE"] > 0):?><span><?=$arItem["SUM"]?></span><?endif;?>
		<span><?=number_format(($arItem["FULL_PRICE"]*$arItem["QUANTITY"]), 0, '.', ' ');?> руб.</span>
		<?if ($arItem["DISCOUNT_PRICE"] > 0):?><span>Экономия: <?=number_format((floor($arItem["DISCOUNT_PRICE"])*$arItem["QUANTITY"]), 0, '.', ' ');?> руб.</span><?endif;?>
	</div>
</div>
<hr />
<?endforeach;?>
<div class="basket_cupon">
<p class="basket_summa"><a class="button" href="" id="BasketRefresh">Пересчитать заказ</a><a class="button" id="order_one_click" href="javascript:void(0)">Оформить в один клик</a></p>
<?if ($arParams["HIDE_COUPON"] != "Y"):?>
		
		
		<p><span><?= GetMessage("STB_COUPON_PROMT") ?></span><input type="text" name="COUPON" value="<?=$arResult["COUPON"]?>" size="20"/></p>
		
<?endif;?>
</div>
<div class="basket_all_order">
	<div class="basket_all_submit"><input type="submit" value="Оформить заказ" id="BasketOrder" name="BasketOrder" id="basketOrderButton1"></div>
	<div class="basket_all_summa">Общая стоимость: <span><?=number_format($arResult["allSum"], 0, '.', ' ');?> руб.</span></div>
	<div></div>
</div>
<div class="clear"></div>



</div>

<script>
$(document).ready(function(){
	$('#clear_cart').click(function() {
		$('.basket_tovar').each(function(){
			$('#basket_form').append('<input type="hidden" value="Y" name="DELETE_'+$(this).find('.del a').attr('id')+'">')
		});
		$('#basket_form').append('<input type="hidden" value="1" name="BasketRefresh">').submit();return false;
	});
	$('#BasketRefresh').click(function() {$('#basket_form').append('<input type="hidden" value="1" name="BasketRefresh">').submit();return false;});
	$('#BasketOrder').click(function() {$('#basket_form').attr("action", "<?=$arParams["PATH_TO_ORDER"]?>");});
	$('#order_one_click').click(function() {$('#basket_form').attr("action", "<?=$arParams["PATH_TO_ORDER"]?>fast/").append('<input type="hidden" value="Y" name="order_one_click">').submit(); return false;});
	$('.basket_tovar').each(function(){
		quantity = $(this).children('div .amt');
		var quantity_val = quantity.children('input');
		quantity.children('.minus').click(function(){if (quantity_val.val() > 0){quantity_val.val(parseInt(quantity_val.val())-1);} else {quantity_val.val(0)}return false;});
		quantity.children('.plus').click(function(){quantity_val.val(parseInt(quantity_val.val())+1);return false;});
		$(this).children('div .del').children('a').click(function(){$('#basket_form').append('<input type="hidden" value="Y" name="DELETE_'+$(this).attr('id')+'"/><input type="hidden" value="1" name="BasketRefresh">').submit();return false;});
	});
});
</script>