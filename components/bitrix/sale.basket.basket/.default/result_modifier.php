<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $key => $arItem)
{
	$element = CIBlockElement::GetByID($arItem["PRODUCT_ID"])->GetNextElement();
	$articul = $element->GetProperty("ARTICUL");
	$arResult["ITEMS"]["AnDelCanBuy"][$key]["ARTICUL"] = $articul["VALUE"];
	$photo = $element->GetProperty("MORE_PHOTO");
	$img = CFile::ResizeImageGet( 
		$photo["VALUE"]["0"], 
		array("width"=>"60", "height"=>"60"), 
		BX_RESIZE_IMAGE_EXACT, 
		true
	);
	if (!empty($img)) 
		$arResult["ITEMS"]["AnDelCanBuy"][$key]["PICTURE"] = array_change_key_case($img, CASE_UPPER);
	//$arResult["ITEMS"]["AnDelCanBuy"][$key]["PICTURE"] = ImgFileResizeFactory::ResizeImgById($photo["VALUE"]["0"], 60, 60, "", ImgFileResizeFactory::CENTER_CROP, true);
}
?>