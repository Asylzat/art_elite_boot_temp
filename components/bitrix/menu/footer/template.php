<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<p><?
$counter = 1; foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?><?if($arItem["SELECTED"]):?><a href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a><?else:?><a class="menu_<?=$counter?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a><?endif?><?$counter++; endforeach?></p>
<?endif?>