<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams["ONLY_IBLOCK"] = $arParams["ONLY_IBLOCK"] == "Y";
if (!empty($arResult)):?>

<div class="left_block">
	<div class="head">Раскраски по номерам</div>			
	<div class="left_block_more">
<?foreach($arResult as $arItem): if($arParams["ONLY_IBLOCK"] && !$arItem["PARAMS"]["FROM_IBLOCK"]) continue?>
<?if ($arItem["PERMISSION"] > "D"):?><p<?if($arItem["SELECTED"]):?> class="selected"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></p><?endif?>
<?endforeach?>

		<div class="left_block_none" id="left_menu">
				<p><a href="">Пейзажи</a> [21]</p>
				<p><a href="">Птицы</a> [21]</p>
				<p><a href="">Рисование красками</a> [21]</p> 
			</div>
</div>		
<div id="left_more"><p><a href="javascript:void(0)">Показать еще</a></p></div>	
</div> 
<?endif?>

