<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?><ul class="nav navbar-nav b-menu"><?
$previousLevel = 0;
foreach($arResult as $arItem):?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?><?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?><?endif?>
	<?if ($arItem["IS_PARENT"]):?>
		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li class="dropdown"><a aria-haspopup="true" data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle root-item podmenu<?if ($arItem["SELECTED"]):?> selected<?else:?><?endif?>" href="<?=$arItem["LINK"]?>">
				<?=$arItem["TEXT"]?> <span class="caret"></span></a>
				<ul class="dropdown-menu"><?else:?><li<?if ($arItem["SELECTED"]):?> class="selected"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a><ul><?endif?><?else:?><?if ($arItem["PERMISSION"] > "D"):?><?if ($arItem["DEPTH_LEVEL"] == 1):?><li><a class="root-item<?if ($arItem["SELECTED"]):?> selected<?else:?><?endif?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li><?else:?><li<?if ($arItem["SELECTED"]):?> class="root-item selected"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li><?endif?><?else:?><?if ($arItem["DEPTH_LEVEL"] == 1):?><li><a href="" class="<?if ($arItem["SELECTED"]):?>selected<?else:?><?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li><?else:?><li><a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li><?endif?><?endif?><?endif?><?$previousLevel = $arItem["DEPTH_LEVEL"];?><?endforeach?>
<?if ($previousLevel > 1)://close last item tags?><?=str_repeat("</ul></li>", ($previousLevel-1) );?><?endif?></ul><?endif?>
