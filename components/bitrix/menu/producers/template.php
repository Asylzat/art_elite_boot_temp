<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams["ONLY_IBLOCK"] = $arParams["ONLY_IBLOCK"] == "Y";
if (!empty($arResult)):?>
<div class="left_type">
	<div class="head pr_icons"><p>Производители</p></div>
	<div class="left_block_type"><?foreach($arResult as $arItem): if($arParams["ONLY_IBLOCK"] && !$arItem["PARAMS"]["FROM_IBLOCK"]) continue?><?if ($arItem["PERMISSION"] > "D"):?><p<?if($arItem["SELECTED"]):?> class="selected"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></p><?endif?><?endforeach?></div>
</div>
<?endif?>