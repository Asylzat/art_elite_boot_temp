<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams["ONLY_IBLOCK"] = $arParams["ONLY_IBLOCK"] == "Y";
if (!empty($arResult)):?>
<ul class="personal_menu">
 <?foreach($arResult as $arItem): if($arParams["ONLY_IBLOCK"] && !$arItem["PARAMS"]["FROM_IBLOCK"]) continue?><?if ($arItem["PERMISSION"] > "D"):?><li<?if($arItem["SELECTED"]):?> class="selected"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li><?endif?><?endforeach?>
</ul>
<?endif?>