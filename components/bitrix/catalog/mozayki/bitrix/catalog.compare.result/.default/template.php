<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
function deleteFromCompare(id)
{
	$('#compare_result').append('<input type="hidden" name="ID[]" value="'+id+'" />').submit();
}
</script>
<div class="catalog_section_text">
<h1>Сравнение</h1>
<div class="catalog-compare-result">
	<noindex><p class="compare_all">
	<?if($arResult["DIFFERENT"]):
		?><a href="<?=htmlspecialcharsbx($APPLICATION->GetCurPageParam("DIFFERENT=N",array("DIFFERENT")))?>" rel="nofollow"><?=GetMessage("CATALOG_ALL_CHARACTERISTICS")?></a><?
	else:
		?><?=GetMessage("CATALOG_ALL_CHARACTERISTICS")?><?
	endif
	?>&nbsp;|&nbsp;<?
	if(!$arResult["DIFFERENT"]):
		?><a href="<?=htmlspecialcharsbx($APPLICATION->GetCurPageParam("DIFFERENT=Y",array("DIFFERENT")))?>" rel="nofollow"><?=GetMessage("CATALOG_ONLY_DIFFERENT")?></a><?
	else:
		?><?=GetMessage("CATALOG_ONLY_DIFFERENT")?><?
	endif?>
	</p></noindex>

<form action="<?=$APPLICATION->GetCurPage()?>" method="get" id="compare_result">
	<table class="compare">
		
		<tr class="compare_del">
			<td>&nbsp;</td>
			<?foreach($arResult["ITEMS"] as $arElement):?>
			<td><a href="javascript:void(0)" onclick="deleteFromCompare(<?=$arElement["ID"]?>)"></td>
			<?endforeach?>
		</tr>
		<tr class="name">
			<th>Название</th>
			<?foreach($arResult["ITEMS"] as $arElement):?>
			<td><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></td>
			<?endforeach?>
		</tr>	
		<tr>
			<td>&nbsp;</td>
			<?foreach($arResult["ITEMS"] as $arElement):?>
			<td><p><a class="photo_report" rel="fancybox-thumb-compare" href="<?=$arElement["DETAIL_PICTURE"]["SRC"]?>"><img class="selected" src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arElement["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="" /></a></p></td>
			<?endforeach?>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<?foreach($arResult["ITEMS"] as $arElement):?>
			<td><noindex><a class="button" href="<?=$arElement["DETAIL_PAGE_URL"]?>">Подробнее</a></noindex></td>
			<?endforeach?>
		</tr>	
		<?foreach($arResult["ITEMS"][0]["PRICES"] as $code=>$arPrice):?>
			<?if($arPrice["CAN_ACCESS"]):?>
			<tr>
				<th><?=$arResult["PRICES"][$code]["TITLE"]?></th>
				<?foreach($arResult["ITEMS"] as $arElement):?>
					<td>
						<?if($arElement["PRICES"][$code]["CAN_ACCESS"]):?>
							<b><?=$arElement["PRICES"][$code]["PRINT_DISCOUNT_VALUE"]?></b>
						<?endif;?>
					</td>
				<?endforeach?>
			</tr>
			<?endif;?>
		<?endforeach;?>
		<?foreach($arResult["SHOW_PROPERTIES"] as $code=>$arProperty):
			$arCompare = Array();
			foreach($arResult["ITEMS"] as $arElement)
			{
				$arPropertyValue = $arElement["DISPLAY_PROPERTIES"][$code]["VALUE"];
				if(is_array($arPropertyValue))
				{
					sort($arPropertyValue);
					$arPropertyValue = implode(" / ", $arPropertyValue);
				}
				$arCompare[] = $arPropertyValue;
			}
			$diff = (count(array_unique($arCompare)) > 1 ? true : false);
			if($diff || !$arResult["DIFFERENT"]):
				if(!empty($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])):
			?>
				<tr>
					<th><?=$arProperty["NAME"]?></th>
					<?foreach($arResult["ITEMS"] as $arElement):?>
					<td><?=(is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]): $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])?></td>
					<?endforeach?>
				</tr>
			<?endif;endif;?>
		<?endforeach;?>
	</table>
	<br />

	<input type="hidden" name="action" value="DELETE_FROM_COMPARE_RESULT" />
	<input type="hidden" id="" name="IBLOCK_ID" value="<?=$arParams["IBLOCK_ID"]?>"/>
</form>
</div>
</div>