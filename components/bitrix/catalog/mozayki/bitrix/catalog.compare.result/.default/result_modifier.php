<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
foreach($arResult["ITEMS"] as $key=>$arItem)
{
	if(empty($arItem["DETAIL_PICTURE"]))
	{
		$rsMorePhoto = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $arItem["ID"], array(), array("CODE" => "MORE_PHOTO", "EMPTY" => "N"));
		if ($morePhoto = $rsMorePhoto->GetNext())
		{
			$arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = ImgFileResizeFactory::ResizeImgById($morePhoto["VALUE"], 120, 120, "", ImgFileResizeFactory::CENTER_CROP, true);
			$arResult["ITEMS"][$key]["DETAIL_PICTURE"] = ImgFileResizeFactory::ResizeImgById($morePhoto["VALUE"], 800, 600, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/wm_b.png");
		}
	}
	if(!empty($arItem["DISPLAY_PROPERTIES"]["COMPLEXITY"]["VALUE"]))
		$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"]["COMPLEXITY"]["DISPLAY_VALUE"] = '<em class="rating_'.intVal($arItem["DISPLAY_PROPERTIES"]["COMPLEXITY"]["VALUE"]).'"></em>';
}
?>