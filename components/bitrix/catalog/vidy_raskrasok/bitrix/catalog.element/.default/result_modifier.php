<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$producer = CIBlockElement::GetByID($arResult["PROPERTIES"]["PRODUCER"]["VALUE"])->GetNext();
$arResult["PROPERTIES"]["PRODUCER"]["VALUE"] = $producer["NAME"];
?>
<?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $cell=>$photo):?>
<?
$file = CFile::GetFileArray($photo);
if ($file["WIDTH"] >= $file["HEIGHT"])
{
	$big = ImgFileResizeFactory::ResizeImgById($photo, 800, 600, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/wm_b.png");
	$small = ImgFileResizeFactory::ResizeImgById($photo, 491, 328, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/wm_s.png", ImgFileResizeFactory::CENTER_CROP, true);
}
else
{
	$big = ImgFileResizeFactory::ResizeImgById($photo, 600, 800, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/wm_b.png");
	$small = ImgFileResizeFactory::ResizeImgById($photo, 328, 491, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/wm_s.png", ImgFileResizeFactory::CENTER_CROP, true);
}
$arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][$cell] = array("BIG"=> $big, "SMALL" => $small);
?>
<?endforeach;?>
<?
$rsPath = GetIBlockSectionPath($arResult["SECTION"]["IBLOCK_ID"], $arResult["SECTION"]["IBLOCK_SECTION_ID"]);
$url = "/catalog/";
while ($path = $rsPath->GetNext())
{
	$arPath[] = $path;
	$url .= $path["CODE"] . "/";
}
$url .= $arResult["SECTION"]["CODE"] . "/";
$arResult["SECTION"]["SECTION_PAGE_URL"] = $url;

$cp = $this->__component; // ������ ����������

if (is_object($cp))
{
	// ������� � arResult ���������� ��� ���� - MY_TITLE � IS_OBJECT
	//$cp->arResult['NAVI_ELEMENT'] = $navi;
	$cp->SetResultCacheKeys(array('DETAIL_PAGE_URL'));
}
?>
 