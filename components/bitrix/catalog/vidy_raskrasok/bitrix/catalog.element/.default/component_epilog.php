<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["PROPERTIES"]["TITLE"]["VALUE"]))
	$APPLICATION->SetPageProperty("title", $arResult["PROPERTIES"]["TITLE"]["VALUE"]);
if (!empty($arResult["PROPERTIES"]["H1"]["VALUE"]))
	$APPLICATION->SetPageProperty("h1", $arResult["PROPERTIES"]["H1"]["VALUE"]);
if (!empty($arResult["PROPERTIES"]["KEYWORDS"]["VALUE"]))
	$APPLICATION->SetPageProperty("keywords", $arResult["PROPERTIES"]["KEYWORDS"]["VALUE"]);
if (!empty($arResult["PROPERTIES"]["DESCRIPTION"]["VALUE"]))
	$APPLICATION->SetPageProperty("description", $arResult["PROPERTIES"]["DESCRIPTION"]["VALUE"]);
?>