<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $PHOTO):
if($PHOTO["SMALL"]["WIDTH"] >= $PHOTO["SMALL"]["HEIGHT"]):?><div class="catalog_img"><?else:?><div class="catalog_img1"><?endif;?><a id="element_<?=$arResult["ID"]?>" class="photos" rel="example1" href="<?=$PHOTO["BIG"]["SRC"]?>"><img src="<?=$PHOTO["SMALL"]["SRC"]?>" width="<?=$PHOTO["SMALL"]["WIDTH"]?>" height="<?=$PHOTO["SMALL"]["HEIGHT"]?>" alt="<?=$arResult["SMALL"]["NAME"]?>" title="<?=$arResult["SMALL"]["NAME"]?>" /></a></div><?endforeach?>

<div class="catalog_text">
	<?if($arResult["PROPERTIES"]["COMPLECT"]["VALUE"]):?>
	<div class="catalog_complect">
	<p>В комплекте:</p>
	<ul>
		 <?foreach($arResult["PROPERTIES"]["COMPLECT"]["VALUE"] as $complect):?><li><?print($complect);?></li><?endforeach?> 
	</ul>
	</div>
	<?endif?>
	<div class="catalog_parametr">
	
	<?if($arResult["PROPERTIES"]["ARTICUL"]["VALUE"]):?><span>Артикул:</span> <?print_r($arResult["PROPERTIES"]["ARTICUL"]["VALUE"]);?><br /><?endif?>
	<?if($arResult["PROPERTIES"]["SIZE"]["VALUE"]):?><span>Размер:</span> <?print_r($arResult["PROPERTIES"]["SIZE"]["VALUE"]);?><br /><?endif?>
	<?if($arResult["PROPERTIES"]["TYPE"]["VALUE"]):?><span>Тип:</span> <?print_r($arResult["PROPERTIES"]["TYPE"]["VALUE"]);?><br /><?endif?>
	<?if($arResult["PROPERTIES"]["PRODUCER"]["VALUE"]):?><span>Производитель:</span> <?print_r($arResult["PROPERTIES"]["PRODUCER"]["VALUE"]);?><?endif?>
	
	</div>	
	<div class="catalog_price">
	<?if($arResult["CAN_BUY"]):?>
		<?if (is_array($arResult["PRICE_MATRIX"])):?>
			<?foreach($arResult["PRICE_MATRIX"]["ROWS"] as $ind => $arRow):?>
				<?foreach($arResult["PRICE_MATRIX"]["COLS"] as $typeID => $arType):?>

							<?if($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"] < $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"])
								echo '<span class="catalog-discount-price">'.FormatCurrency($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]).'</span> <span class="catalog-price">'.FormatCurrency($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"], $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
							else
								echo '<span class="catalog-price">'.FormatCurrency($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
							?>

				<?endforeach?>
				<br />
				<noindex>
				<a href="<?echo $arResult["ADD_URL"]?>" rel="nofollow" onclick="loadAddCartMessage(<?=$arResult["ID"]?>)"><?=GetMessage("CATALOG_ADD")?></a>
				</noindex>
				<?endforeach?>
				<?if (count($arResult["PRICE_MATRIX"]["ROWS"]) > 0):?></br><?endif;?>
		<?endif;?>
	<?else:?>
	<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
	<?endif;?>
	</div>
 </div>
 
<?=$arResult["DETAIL_TEXT"];?>

<div class="clear"></div>

 