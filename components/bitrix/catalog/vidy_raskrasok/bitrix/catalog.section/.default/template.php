<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?if (count($arResult["ROWS"])):?>
<div class="sort_top">

<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><?endif;?> 

<div>Сортировать по: цене <?=SortingEx("catalog_PRICE_1")?> названию <?=SortingEx("NAME")?></div>

</div>
<div class="clear"></div>
<hr class="sort_topline" />
<div class="tovar_main">
<table>
<?foreach($arResult["ROWS"] as $cell=>$rowElements):?>
<tr class="element_name">
<?	foreach($rowElements as $key=>$arElement):?>
	<?if ($key == 3):?><td class="last_in_line"><?else:?><td><?endif;?>
		<p class="tovar_main_img"><a id="element_<?=$arElement["ID"]?>" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></p>
	</td>
<?	endforeach;?>
</tr>
<tr>
<?	foreach($rowElements as $key=>$arElement):?>
	<?if ($key == 3):?><td class="last_in_line"><?else:?><td><?endif;?>
		<p class="tovar_main_img"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?if($arElement["PREVIEW_PICTURE"]["SRC"]):?><img src="<?print_r($arElement["PREVIEW_PICTURE"]["SRC"]);?>" width="<?print_r($arElement["PREVIEW_PICTURE"]["WIDTH"]);?>" height="<?print_r($arElement["PREVIEW_PICTURE"]["HEIGHT"]);?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /><?endif?></a></p>
	</td>
<?	endforeach;?>
</tr>
<tr>
	<?	foreach($rowElements as $key=>$arElement):?>
	<?if ($key == 3):?><td class="last_in_line"><?else:?><td><?endif;?>
		<p class="tovar_main_price">
		<?if($arElement["CAN_BUY"]):?>
			<?if (is_array($arElement["PRICE_MATRIX"])):?>
				<?foreach($arElement["PRICE_MATRIX"]["ROWS"] as $ind => $arRow):?>
					<?foreach($arElement["PRICE_MATRIX"]["COLS"] as $typeID => $arType):?>

								<?if($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"] < $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"])
									echo '<span class="catalog-discount-price">'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]).'</span> <span class="catalog-price">'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
								else
									echo '<span class="catalog-price">'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
								?>

					<?endforeach?>
					<noindex>
					<a href="<?echo $arElement["ADD_URL"]?>" rel="nofollow" onclick="loadAddCartMessage(<?=$arElement["ID"]?>)"><?=GetMessage("CATALOG_ADD")?></a>
					</noindex>
					<?endforeach?>
					<?if (count($arElement["PRICE_MATRIX"]["ROWS"]) > 0):?></br><?endif;?>
			<?endif;?>
		<?elseif (count($arElement)>0):?>
		<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
		<?endif;?>
		</p>
	</td>
<?	endforeach;?>
</tr>
<tr>
<?	foreach($rowElements as $key=>$arElement):?>
	<?if ($key == 3):?><td class="last_in_line"><?else:?><td><?endif;?>
		<?if (count($arElement["DISPLAY_PROPERTIES"])):?>
		<p class="tovar_main_set"><?foreach($arElement["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?><span><?=$arProperty["NAME"]?>:</span> <? if(is_array($arProperty["DISPLAY_VALUE"])) echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]); else echo $arProperty["DISPLAY_VALUE"];?><br /><?endforeach?></p>
		<?endif;?>
	</td>
<?	endforeach;?>
</tr>
<tr><td colspan="<?=$arParams["LINE_ELEMENT_COUNT"]?>"><div class="clear"></div></td></tr>
<?endforeach;?>
</table>	
</div>
<hr class="sort_topline" /> 
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><div class="sort_top"><?=$arResult["NAV_STRING"]?></div><?endif;?>
<?endif;?>
 <?if($arResult["~DESCRIPTION"]):?><?=$arResult["~DESCRIPTION"]?><?endif?>