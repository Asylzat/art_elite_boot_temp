<?
$rows = array(); 
$rowElements = array();
$count = 0;
global $USER;
foreach($arResult["ITEMS"] as $cell=>$arElement)
{
	$element = CIBlockElement::GetByID($arElement["ID"])->GetNext();
	$arResult["ITEMS"][$cell]["DETAIL_PAGE_URL"] = $element["DETAIL_PAGE_URL"];
	$count++;
	$producer = CIBlockElement::GetByID($arElement["PROPERTIES"]["PRODUCER"]["VALUE"])->GetNext();
	$arResult["ITEMS"][$cell]["PROPERTIES"]["PRODUCER"]["VALUE"] = $producer["NAME"];
	if (intVal($arElement["PREVIEW_PICTURE"]["ID"]) > 0)
	{
		$arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = ImgFileResizeFactory::ResizeImgById($arElement["PREVIEW_PICTURE"]["ID"], 160, 200, "", ImgFileResizeFactory::CENTER_CROP, true);
	}
	else
	{
		foreach($arElement["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $photo)
		{
			$arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = ImgFileResizeFactory::ResizeImgById($photo, 160, 200, "", ImgFileResizeFactory::CENTER_CROP, true);
			//$arResult["ITEMS"][$cell]["DETAIL_PICTURE"] = ImgFileResizeFactory::ResizeImgById($photo, 800, 600, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/watermark.png");
			break;
		}
	}
	if ($count == $arParams["LINE_ELEMENT_COUNT"])
	{
		$count = 0;
		$rowElements[] = $arResult["ITEMS"][$cell];
		$rows[] = $rowElements;
		$rowElements = array();
	}
	else
	{
		$rowElements[] = $arResult["ITEMS"][$cell];
	}	
		
}
if ($count > 0)
{
	for($i = $count; $i<$arParams["LINE_ELEMENT_COUNT"]; $i++)
		$rowElements[] = array();
	$rows[] = $rowElements;
}
$arResult["ROWS"] = $rows;
/*
foreach($arResult["ITEMS"] as $cell1 => $arElement)
{
		$arElement["BUY_URL"] = htmlspecialchars($arParams["BASKET_URL"]."?".$arParams["ACTION_VARIABLE"]."=BUY&".$arParams["PRODUCT_ID_VARIABLE"]."=".$arElement["ID"]);
		$arElement["ADD_URL"] = htmlspecialchars($arParams["BASKET_URL"]."?".$arParams["ACTION_VARIABLE"]."=ADD2BASKET&".$arParams["PRODUCT_ID_VARIABLE"]."=".$arElement["ID"]."&backurl=".$APPLICATION->GetCurPageParam("", Array("ADD2BASKET", "BUY")));
		$arElement["CAN_BUY"] = "Y";

		$arResult["ITEMS"][$cell1] = $arElement;
}*/
$cp = $this->__component; // объект компонента

if (is_object($cp))
{
	// добавим в arResult компонента два поля - MY_TITLE и IS_OBJECT
	$cp->arResult["LIST_PAGE_URL"] = $arResult["LIST_PAGE_URL"];
	$path = array();
	$rsPath = GetIBlockSectionPath($arResult["IBLOCK_ID"], $arResult["IBLOCK_SECTION_ID"]);
	while($arPath=$rsPath->GetNext())
	{
		$path[] = $arPath;
	}
	$cp->arResult["TITLE"] = $arResult["UF_TITLE"];
	$cp->arResult["H1"] = $arResult["UF_H1"];
	$cp->arResult["KEYWORDS"] = $arResult["UF_KEYWORDS"];
	$cp->arResult["DESCRIPTION"] = $arResult["UF_DESCRIPTION"];
	//Добавляем ключи arResult, которые мы добавили в result_modifier.php и которые необходимо сохранить в кеше.
    $cp->SetResultCacheKeys(array("LIST_PAGE_URL","PATH","TITLE","H1","KEYWORDS","DESCRIPTION"));
	// сохраним их в копии arResult, с которой работает шаблон (с учетом версии main 10.0 и выше)
}



?>
