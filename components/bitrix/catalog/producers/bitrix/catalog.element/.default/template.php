<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$elementObj = CIBlockElement::GetList(
	array("rand"=>"ASC"),
	array("IBLOCK_ID" => "1", "ACTIVE"=>"Y", "PROPERTY_PRODUCER" => $arResult["ID"]),
	false,
	array("nTopCount" => "1")
);
$showH1 = false;
if ($element = $elementObj->GetNext())
{
	$FILTER_NAME = "arrFilter_producer";
	global ${$FILTER_NAME};
	${$FILTER_NAME} = array();
	${$FILTER_NAME}["PROPERTY"]["PRODUCER"]=$arResult["ID"];
	$showH1 = true;
}
$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"producer_goods",
	Array(
		//"H1" => (($showH1)?((!empty($arResult["PROPERTIES"]["H1"]["VALUE"])) ? $arResult["PROPERTIES"]["H1"]["VALUE"] : $arResult["NAME"]) : ""),
		"H1" => ((!empty($arResult["PROPERTIES"]["H1"]["VALUE"])) ? $arResult["PROPERTIES"]["H1"]["VALUE"] : $arResult["~NAME"]),
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "1",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
		"ELEMENT_SORT_FIELD" => "CATALOG_AVAILABLE",
		"ELEMENT_SORT_ORDER" => "DESC",
		"ELEMENT_SORT_FIELD2" => "rand",
		"ELEMENT_SORT_ORDER2" => "DESC",
		"FILTER_NAME" => "arrFilter_producer",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"PAGE_ELEMENT_COUNT" => "12",
		"LINE_ELEMENT_COUNT" => "6",
		"PROPERTY_CODE" => array(0=>"ARTICUL",1=>"SIZE",2=>"TYPE",3=>"COMPLEXITY",),
		"OFFERS_LIMIT" => "5",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"CACHE_FILTER" => "N",
		"PRICE_CODE" => array(0=>"RUB",),
		"USE_PRICE_COUNT" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_PROPERTIES" => array(),
		"USE_PRODUCT_QUANTITY" => "N",
		"CONVERT_CURRENCY" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "������",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "forum",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
false
);?>
<?if(!empty($arResult["DETAIL_TEXT"])):?>
<div class="main_sale"> 
<?=$arResult["DETAIL_TEXT"]?>
</div>
<?endif;?>
