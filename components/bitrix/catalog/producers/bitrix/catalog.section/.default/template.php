<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="main_sale">
<h1>Производители</h1>

<ul>
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
<li><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></li>
<?endforeach;?>
</ul>

</div>