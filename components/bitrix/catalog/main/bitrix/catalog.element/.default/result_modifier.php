<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$producer = CIBlockElement::GetByID($arResult["PROPERTIES"]["PRODUCER"]["VALUE"])->GetNext();
$arResult["PROPERTIES"]["PRODUCER"]["VALUE"] = $producer["NAME"];
$size = CIBlockElement::GetByID($arResult["PROPERTIES"]["SIZE2"]["VALUE"])->GetNext();
$arResult["PROPERTIES"]["SIZE"]["VALUE"] = $size["NAME"];
if ($arResult["PROPERTIES"]["SET"]["VALUE"])
	$arResult["PROPERTIES"]["COMPLECT"]["VALUE"] = explode(";", trim($arResult["PROPERTIES"]["SET"]["VALUE"], ";"));
?>
<?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $cell=>$photo):?>
<?
$file = CFile::GetFileArray($photo);
if ($file["WIDTH"] >= $file["HEIGHT"])
{
	$big = ImgFileResizeFactory::ResizeImgById($photo, 800, 600, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/wm_b.png");
	$small = ImgFileResizeFactory::ResizeImgById($photo, 445, 445, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/wm_s.png", ImgFileResizeFactory::CENTER_CROP, true);
}
else
{
	$big = ImgFileResizeFactory::ResizeImgById($photo, 600, 800, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/wm_b.png");
	$small = ImgFileResizeFactory::ResizeImgById($photo, 445, 445, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/wm_s.png", ImgFileResizeFactory::CENTER_CROP, true);
}
$arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][$cell] = array("BIG"=> $big, "SMALL" => $small);
?>
<?endforeach;?>
<?
$rsPath = GetIBlockSectionPath($arResult["SECTION"]["IBLOCK_ID"], $arResult["SECTION"]["IBLOCK_SECTION_ID"]);
$url = "/catalog/";
while ($path = $rsPath->GetNext())
{
	$arPath[] = $path;
	$url .= $path["CODE"] . "/";
}
$url .= $arResult["SECTION"]["CODE"] . "/";
$arResult["SECTION"]["SECTION_PAGE_URL"] = $url;

$cp = $this->__component; // ������ ����������

if (is_object($cp))
{
	// ������� � arResult ���������� ��� ���� - MY_TITLE � IS_OBJECT
	//$cp->arResult['NAVI_ELEMENT'] = $navi;
	$cp->arResult["PATH"] = $arResult["SECTION"]["PATH"];
	$cp->arResult["~NAME"] = $arResult["~NAME"];
	$cp->SetResultCacheKeys(array('DETAIL_PAGE_URL',"PATH","~NAME"));
}

$elementObjs = CIBlockElement::GetList(
	array("SORT"=>"ASC"),
	array(
		"ACTIVE" => "Y",
		"IBLOCK_ID" => 14,
		"PROPERTY_ORIGINAL" => $arResult["ID"]	
	),
	false,
	false
);
if ($element = $elementObjs->GetNext())
{
	$arResult["PAINT"] = ImgFileResizeFactory::ResizeImgById($element["PREVIEW_PICTURE"], 800, 600, $_SERVER["DOCUMENT_ROOT"] . "/upload/resize_cache/wm_b.png");
	
}
/*
print("<pre>");
print_r($arResult);
print("</pre>");
*/
?>
 