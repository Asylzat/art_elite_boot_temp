<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$cp = $this->__component; // ������ ����������

if (is_object($cp))
{
	// ������� � arResult ���������� ��� ���� - MY_TITLE � IS_OBJECT
	$cp->arResult['PROPERTIES'] = $arResult['PROPERTIES'];
	$cp->SetResultCacheKeys(array('PROPERTIES'));
}
$arResult['ITEMS'] = array();
if (count($arResult['PROPERTIES']["GOODS"]["VALUE"]) > 0)
{
	$elementsObj = CIBlockElement::GetList(
		array("SORT"=>"ASC"),
		array("IBLOCK_ID" => 1, "ID"=>$arResult['PROPERTIES']["GOODS"]["VALUE"])
	);
	while($elementObj = $elementsObj->GetNextElement())
	{
		$element = $elementObj->GetFields();
		$element["PROPERTIES"] = $elementObj->GetProperties();
		
		$element["PRICE_MATRIX"] = CatalogGetPriceTableEx($element["ID"], 0, array(1), 'Y');
		
		
		$producer = CIBlockElement::GetByID($element["PROPERTIES"]["PRODUCER"]["VALUE"])->GetNext();
		$element["PROPERTIES"]["PRODUCER"]["VALUE"] = $producer["NAME"];
		if (intVal($element["PREVIEW_PICTURE"]) > 0)
		{
			$element["PREVIEW_PICTURE"] = ImgFileResizeFactory::ResizeImgById($element["PREVIEW_PICTURE"], 120, 120, "", ImgFileResizeFactory::CENTER_CROP, true);
		}
		else
		{
			foreach($element["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $photo)
			{
				$element["PREVIEW_PICTURE"] = ImgFileResizeFactory::ResizeImgById($photo, 120, 120, "", ImgFileResizeFactory::CENTER_CROP, true);
				break;
			}
		}
		$arResult['ITEMS'][] = $element;
		
	}
}

?>
 