<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="news_data"><?=strtolower(FormatDate("d F Y", MakeTimeStamp($arResult["DATE_ACTIVE_FROM"])))?></div>
<div class="news_prew"><?=$arResult["PREVIEW_TEXT"]?></div>

<?=$arResult["DETAIL_TEXT"]?>
<?if(count($arResult["ITEMS"]) > 0):?>
</div>
<div class="main_new">

<?foreach($arResult["ITEMS"] as $key=>$arElement):?><div><div><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?if($arElement["PREVIEW_PICTURE"]["SRC"]):?><img src="<?print_r($arElement["PREVIEW_PICTURE"]["SRC"]);?>" width="<?print_r($arElement["PREVIEW_PICTURE"]["WIDTH"]);?>" height="<?print_r($arElement["PREVIEW_PICTURE"]["HEIGHT"]);?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /><?endif?></a></div><div><p class="tovar"><a id="element_<?=$arElement["ID"]?>" title="<?=$arElement["NAME"]?>" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></p><?if($arElement["PROPERTIES"]["SIZE"]["VALUE"]):?><p>Размер: <?print($arElement["PROPERTIES"]["SIZE"]["VALUE"]);?>.</p><?endif?><?if($arElement["PROPERTIES"]["COMPLEXITY"]["VALUE"]):?><p><em class="rating_<?print($arElement["PROPERTIES"]["COMPLEXITY"]["VALUE"]);?>"></em> Сложность:</p><?endif?><?if($arElement["PROPERTIES"]["TYPE"]["VALUE"]):?><p>Тип: <?print($arElement["PROPERTIES"]["TYPE"]["VALUE"]);?></p><?endif?><p><label for="<?=$arElement["ID"]?>"><input type="checkbox" id="<?=$arElement["ID"]?>" onclick="AddToCompare(this);"> <span>Сравнить</span></label></p></div><p class="main_price"><?if (is_array($arElement["PRICE_MATRIX"])):?><?foreach($arElement["PRICE_MATRIX"]["ROWS"] as $ind => $arRow):?><?foreach($arElement["PRICE_MATRIX"]["COLS"] as $typeID => $arType):?><?if($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"] < $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"])
									echo '<em>Цена: <span>'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]).'</span></em> <span>'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
								else
									echo '<em>Цена:</em> <span>'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
								?><?endforeach?><?endforeach?><?endif;?></p><p><a class="main_more" href="<?=$arElement["DETAIL_PAGE_URL"]?>">Подробнее</a><?if($arElement["PRICE_MATRIX"]["CAN_BUY"]):?><!--noindex--><a href="<?echo $arElement["ADD_URL"]?>" class="main_by addToCart" rel="nofollow">В корзину</a><!--/noindex--><?else:?><a  class="main_by_none" rel="nofollow" >(нет на складе)</a><?endif;?></p></div><?	endforeach;?>

<?endif;?>