<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
<div class="news">
	<div>
		<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
		<img src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arElement["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" />
		<?endif;?>
		<p class="name"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a> <span><?=strtolower(FormatDate("d F Y", MakeTimeStamp($arElement["DATE_ACTIVE_FROM"])))?></span></p>
		<p><?=$arElement["~PREVIEW_TEXT"]?><?if($arElement["DETAIL_TEXT"]):?> <em class="news_more"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>">Подробнее</a> &raquo;</em><?endif?></p>			
	</div>
	<hr />
</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><br /><?=$arResult["NAV_STRING"]?><?endif;?>
