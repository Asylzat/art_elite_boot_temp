<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["PROPERTIES"]["TITLE"]["VALUE"]))
	$APPLICATION->SetPageProperty("title", $arResult["PROPERTIES"]["TITLE"]["VALUE"]);
if (!empty($arResult["PROPERTIES"]["H1"]["VALUE"]))
	$APPLICATION->SetPageProperty("h1", $arResult["PROPERTIES"]["H1"]["VALUE"]);
if (!empty($arResult["PROPERTIES"]["KEYWORDS"]["VALUE"]))
	$APPLICATION->SetPageProperty("keywords", $arResult["PROPERTIES"]["KEYWORDS"]["VALUE"]);
if (!empty($arResult["PROPERTIES"]["DESCRIPTION"]["VALUE"]))
	$APPLICATION->SetPageProperty("description", $arResult["PROPERTIES"]["DESCRIPTION"]["VALUE"]);
$APPLICATION->SetPageProperty("show_oplata_dostavka", "N");
foreach($arResult["PATH"] as $section)
{
	$APPLICATION->AddChainItem($section["NAME"], $section["SECTION_PAGE_URL"]);
}
$APPLICATION->AddChainItem($arResult["NAME"]);
$APPLICATION->SetPageProperty("itempropvalue", "name");
//print($arResult["~NAME"]);
?>