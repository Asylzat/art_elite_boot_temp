<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (count($arResult["ROWS"])):?>
<div class="sort">Сортировать по: <span>названию <?=SortingEx("NAME")?></span> <span>цене <?=SortingEx("catalog_PRICE_1")?></span></div>
<?if(!empty($arResult["SUB_HEADERS"])):?>
<div class="main_new">
<?foreach($arResult["SUB_HEADERS"] as $keyHeader => $header):?>
<div class="subsection">
<?=$header?>:
<?foreach($arResult["SUB_SECTIONS"][$keyHeader] as $key=>$arSection):?><?if($key > 0):?>, <?endif;?><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=strtolower($arSection["~NAME"])?></a><?endforeach?>
</div>
<?endforeach?>
</div>
<?endif;?>
<?
$first=true;
foreach($arResult["ROWS"] as $cell=>$rowElements):?>
<div class="main_new">
<?if($first):?>	
	<? if(CSite::Indir('/vyshivka_lentami/index.php')): ?><h1>Вышивка лентами</h1><?else:?><h1><?=((!empty($arResult["UF_H1"])) ? $arResult["UF_H1"] : $arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"])?></h1><?endif?>
<?endif?>

<?foreach($rowElements as $key=>$arElement): if (count($arElement)>0):?><div>

		<div><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?if($arElement["PREVIEW_PICTURE"]["SRC"]):?><img src="<?print_r($arElement["PREVIEW_PICTURE"]["SRC"]);?>" width="<?print_r($arElement["PREVIEW_PICTURE"]["WIDTH"]);?>" height="<?print_r($arElement["PREVIEW_PICTURE"]["HEIGHT"]);?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /><?endif?></a></div>
		
		<div>
			<p class="tovar"><a id="element_<?=$arElement["ID"]?>" title="<?=$arElement["NAME"]?>" href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></p>			
			<?if($arElement["PROPERTIES"]["SIZE2"]["VALUE"]):?><p>Размер: <?print($arElement["PROPERTIES"]["SIZE"]["VALUE"]);?>.</p><?endif?>
			<?if($arElement["PROPERTIES"]["COMPLEXITY"]["VALUE"]):?><p><em class="rating_<?print($arElement["PROPERTIES"]["COMPLEXITY"]["VALUE"]);?>"></em> Сложность:</p><?endif?>
			<?if($arElement["PROPERTIES"]["TYPE"]["VALUE"]):?><p>Тип: <?print($arElement["PROPERTIES"]["TYPE"]["VALUE"]);?></p><?endif?>
			<p><label for="<?=$arElement["ID"]?>"><input type="checkbox" id="<?=$arElement["ID"]?>" onclick="AddToCompare(this);"> <span>Сравнить</span></label></p>
		</div> 
		
		<p class="main_price"> 
			<?if (is_array($arElement["PRICE_MATRIX"])):?>
				<?foreach($arElement["PRICE_MATRIX"]["ROWS"] as $ind => $arRow):?>
					<?foreach($arElement["PRICE_MATRIX"]["COLS"] as $typeID => $arType):?>

								<?if($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"] < $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"])
									echo '<em>Цена: <span>'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]).'</span></em> <span>'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
								else
									echo '<em>Цена:</em> <span>'.FormatCurrency($arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arElement["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
								?>

					<?endforeach?>
					
					
					<?endforeach?>
					<?if (count($arElement["PRICE_MATRIX"]["ROWS"]) > 0):?></br><?endif;?>
			<?endif;?>
		</p>
		<p><a class="main_more" href="<?=$arElement["DETAIL_PAGE_URL"]?>">Подробнее</a><?if($arElement["CAN_BUY"]):?><!--noindex--><a href="<?echo $arElement["ADD_URL"]?>" class="main_by addToCart" rel="nofollow" ><?=GetMessage("CATALOG_ADD")?></a><!--/noindex--><?else:?><a  class="main_by_none" rel="nofollow" ><?=GetMessage("CATALOG_NOT_AVAILABLE")?></a><?endif;?></p></div><?endif;?><?endforeach;?>
		
		
		<?if((count($arResult["ROWS"])-1 == $cell)&&($arParams["DISPLAY_BOTTOM_PAGER"])):?><?=$arResult["NAV_STRING"]?><?endif;?>
		
</div>

<?if($first):?><?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("includes/main_action.php"), Array(), Array("MODE"=>"html") );?><?endif;?>

<?
$first=false;
endforeach;?>
		 
<?else:?>
<?if(!empty($arResult["SUB_HEADERS"])):?>
<div class="main_new">
<?foreach($arResult["SUB_HEADERS"] as $keyHeader => $header):?>
<div class="subsection">
<?=$header?>:
<?foreach($arResult["SUB_SECTIONS"][$keyHeader] as $key=>$arSection):?><?if($key > 0):?>, <?endif;?><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=strtolower($arSection["~NAME"])?></a><?endforeach?>
</div>
<?endforeach?>
</div>
<?endif;?>
<div class="main_new">
<h1><?=((!empty($arResult["UF_H1"])) ? $arResult["UF_H1"] : $arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"])?></h1>
</div>
<?endif;?>



  
 