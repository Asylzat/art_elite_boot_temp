<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$producer = CIBlockElement::GetByID($arResult["PROPERTIES"]["PRODUCER"]["VALUE"])->GetNext();
$arResult["PROPERTIES"]["PRODUCER"]["VALUE"] = $producer["NAME"];
if($arResult["PROPERTIES"]["SIZE2"]["VALUE"])
{
	$size = CIBlockElement::GetByID($arResult["PROPERTIES"]["SIZE2"]["VALUE"])->GetNext();
	$arResult["PROPERTIES"]["SIZE2"]["VALUE"] = $size["NAME"];
}
$arWaterMark = Array(
	array(
		"name" => "watermark",
		"position" => "bottomright", // ���������
		"type" => "image",
		"size" => "real",
		"file" => $_SERVER["DOCUMENT_ROOT"].'/upload/resize_cache/wm_b.png', // ���� � ��������
		"fill" => "exact",
	)
);
?>
<?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $cell=>$photo):?>
<?
$file = CFile::GetFileArray($photo);
if ($file["WIDTH"] >= $file["HEIGHT"])
{
	$img = CFile::ResizeImageGet( 
		$photo, 
		array("width"=>"800", "height"=>"600"), 
		BX_RESIZE_IMAGE_EXACT, 
		true,
		$arWaterMark
	);
	if (!empty($img)) 
		$big = array_change_key_case($img, CASE_UPPER);
}
else
{
	$img = CFile::ResizeImageGet( 
		$photo, 
		array("width"=>"600", "height"=>"800"), 
		BX_RESIZE_IMAGE_EXACT, 
		true,
		$arWaterMark
	);
	if (!empty($img))
		$big = array_change_key_case($img, CASE_UPPER);
}
$img = CFile::ResizeImageGet( 
	$photo, 
	array("width"=>"445", "height"=>"445"), 
	BX_RESIZE_IMAGE_EXACT, 
	true,
	$arWaterMark
);
if (!empty($img)) 
	$small = array_change_key_case($img, CASE_UPPER);
$arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][$cell] = array("BIG"=> $big, "SMALL" => $small);
?>
<?endforeach;?>
<?
$rsPath = GetIBlockSectionPath($arResult["SECTION"]["IBLOCK_ID"], $arResult["SECTION"]["IBLOCK_SECTION_ID"]);
$url = "/catalog/";
while ($path = $rsPath->GetNext())
{
	$arPath[] = $path;
	$url .= $path["CODE"] . "/";
}
$url .= $arResult["SECTION"]["CODE"] . "/";
$arResult["SECTION"]["SECTION_PAGE_URL"] = $url;

$cp = $this->__component; // ������ ����������

if (is_object($cp))
{
	// ������� � arResult ���������� ��� ���� - MY_TITLE � IS_OBJECT
	//$cp->arResult['NAVI_ELEMENT'] = $navi;
	$cp->SetResultCacheKeys(array('DETAIL_PAGE_URL'));
}
?>
 