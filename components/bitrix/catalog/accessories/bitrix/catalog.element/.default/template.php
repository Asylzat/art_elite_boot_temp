<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $PHOTO):?><div class="element_big_photo"><a id="example1" title="<?=$arResult["NAME"]?>" href="<?=$PHOTO["BIG"]["SRC"]?>"><span></span><img src="<?=$PHOTO["SMALL"]["SRC"]?>" width="<?=$PHOTO["SMALL"]["WIDTH"]?>" height="<?=$PHOTO["SMALL"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" /></a></div><?endforeach?>

<div class="element_anons">
	
	
	<script>
		$(function() {
		 
		$( "#tabs" ).tabs({
		select: function(event, ui){
			if (ui.index == 0)
			{$( "#tabs_menu").removeClass('ul_tab1').addClass('ul_tab0');}
			else
			{$( "#tabs_menu").removeClass('ul_tab0').addClass('ul_tab1');}
		} 
		});
		});
	</script>
	
	
		<div id="tabs">
			<ul id="tabs_menu" class="ul_tab0"><li><a href="#tabs-1">Характеристики</a></li></ul>
		
			<div id="tabs-1">
				<div class="slider_set">		
					
					<?if($arResult["PROPERTIES"]["ARTICUL"]["VALUE"]):?><p>Артикул: <span><?print_r($arResult["PROPERTIES"]["ARTICUL"]["VALUE"]);?></span></p><?endif?>
					<?if($arResult["PROPERTIES"]["SIZE2"]["VALUE"]):?><p>Рзмер: <span><?print_r($arResult["PROPERTIES"]["SIZE2"]["VALUE"]);?></span></p><?endif?>
					<?if($arResult["PROPERTIES"]["PRODUCER"]["VALUE"]):?><p>Производитель: <span><?print_r($arResult["PROPERTIES"]["PRODUCER"]["VALUE"]);?></span></p><?endif?>
					 
				</div>
				<div class="clear"></div>			
			</div>
			
		 
			 
		</div>
	 
		
		<?if (is_array($arResult["PRICE_MATRIX"])):?>
			<?foreach($arResult["PRICE_MATRIX"]["ROWS"] as $ind => $arRow):?>
			
			  
				<div class="element_allbutton">
				<?if($arResult["CAN_BUY"]):?><p class="element_button"><!--noindex--><a class="main_by addToCart" rel="nofollow" href="<?echo $arResult["ADD_URL"]?>" rel="nofollow"><?=GetMessage("CATALOG_ADD")?></a><!--/noindex--></p><?else:?><p class="element_button"><a class="main_by_none" rel="nofollow" ><?=GetMessage("CATALOG_NOT_AVAILABLE")?></a></p><?endif;?>
				<?if($arResult["CAN_BUY"]):?><p class="element_click"><!--noindex--><a class="button_on addToCartOneClick" rel="nofollow" href="<?echo $arResult["ADD_URL"]?>" rel="nofollow">Оформить в один клик</a></p><?endif;?><!--/noindex-->
				</div>
				
			
				<?foreach($arResult["PRICE_MATRIX"]["COLS"] as $typeID => $arType):?>

							<?if($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"] < $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"])
								echo '<p class="main_price"><em>Цена: <span>'.FormatCurrency($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]).'</span></em> <span>'.FormatCurrency($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"], $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
							else
								echo '<p class="main_price"><em>Цена:</em> <span>'.FormatCurrency($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
							?>

				<?endforeach?>
				 
				
				 
				
				
				<?endforeach?> 
		<?endif;?>
		
		
		<div class="element_stock">
			<p><?if($arResult["CAN_BUY"]):?><span class="yes">Товар в наличии.</span><?else:?><span class="no"><?=GetMessage("CATALOG_NOT_AVAILABLE")?></span><?endif;?></p>
			<?if($arResult["CAN_BUY"]):?><p>Заказать можно по телефону <span id="ya-phone-3">8 (495) 641-67-49</span></p><?else:?><p class="element_send"><a title="Нажмите для получения уведомлений на email о приходе товара" href="javascript:void(0)" data-elementid="<?=$arResult["ID"]?>">Уведомить</a></p><?endif;?>
		</div>

		 <?if($arResult["DETAIL_TEXT"]):?>
<div class="element_text">	
<p class="head">Описание</p>	
	<?=$arResult["DETAIL_TEXT"];?>
</div>
<?endif;?>
		
		
	</div>
	
 


 <div class="clear"></div>

