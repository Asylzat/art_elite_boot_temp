<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

	<div class="catalog_section_text">
	
	<h1>Вопросы и ответы</h1>
	
	<p class="faq_all"><a id="faq_hide_all" href=""><span>Свернуть все</span></a><a id="faq_show_all" href=""><span>Развернуть все</span></a></p>
	
<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><?endif;?>

	<div class="clear"></div>
<div class="article faq">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div>
	 
		
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<p><b><?echo $arItem["NAME"]?></b></p>
		<?endif;?>
		
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?><p class="faq_text"><?echo $arItem["PREVIEW_TEXT"];?></p><?endif;?>
		 
		 
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
			<?else:?>
				<p class="article_more"><span></span><em><a href="#"><span>Читать подробнее</span></a></em></p>
			<?endif;?>
		<?endif;?>		 
		 
	</div>
<?endforeach;?>
</div>
<script>
	function setLinkText(element)
	{
		if ($(element).html() == "<span>Читать подробнее</span>")
		{$(element).html("<span>Свернуть</span>");} 
		else 
		{$(element).html("<span>Читать подробнее</span>");}
	}
	$(".faq .faq_text").hide();
	$("#faq_hide_all").click(function(){
		$(".faq .faq_text").hide("slow", function() {
			$(".article_more a").each(function(){$(this).html("<span>Читать подробнее</span>");});
		});
		return false;
	});
	$("#faq_show_all").click(function(){
		$(".faq .faq_text").show("slow", function() {
			$(".article_more a").each(function(){$(this).html("<span>Свернуть</span>");});
		});
		return false;
	});
	$(".article_more").each(function(){
		var text = $(this).parent().find(".faq_text");
		$(this).find("a").click(function(){
			setLinkText(this);
			text.slideToggle("slow");
			return false;
		});
	});
</script>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><?=$arResult["NAV_STRING"]?><?endif;?>

</div>
