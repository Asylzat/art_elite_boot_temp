<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="poisk"><form action="<?=$arResult["FORM_ACTION"]?>" id="search" method="get"><input class="forma_input" id="q" name="q" placeholder="Поиск по сайту"><input class="forma_submit" type="submit" name="s" value=""></form></div>
<script>
$('#search').find('.forma_input').hover(function(event){
	$('#quick_search').show();
	event.stopPropagation();
});
$('#search').find('.forma_input').click(function(event){
	$('#quick_search').show();
	event.stopPropagation();
});
$('#search').find('.forma_input').keyup(function(event){
	BX.ajax.post(
		"<?=SITE_TEMPLATE_PATH?>/includes/top_poisk_ayax.php",
		$('#search').serialize(),
		function(data){
			$("#quick_search").html(data);
			$('.quickSearchAddToCart').each(function(){
				$(this).unbind("click");
				$(this).bind("click", function(){
					BX.ajax.post(
						$(this).attr('href'),
						"",
						function(){
							BX.ajax.post(
								"<?=SITE_TEMPLATE_PATH?>/includes/top_cart.php",
								"",
								function(data){
									$("#top_cart").html(data);
								}
							);	
						}
					);
					return false;
				});
			});
			$('#quick_search').show();
		}
	)
});
$('html').click(function(){
	$('#quick_search').hide();
});
$('#quick_search').click(function(event){
	$('#quick_search').show();
	event.stopPropagation();
});
</script>