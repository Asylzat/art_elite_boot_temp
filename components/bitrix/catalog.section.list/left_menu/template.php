<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (count($arResult["SECTIONS"])):?>
<div class="left_block slide_menu">
	<div class="head">Раскраски по номерам</div>			
	<div class="left_block_more">
<?
$counter = 0;
foreach($arResult["SECTIONS"] as $key=>$arSection):
if($arSection["UF_NOT_SHOW_IN_MENU"] == 5) continue;
if($counter == 10):?>
	<div class="left_block_none left_menu" id="left_menu">
<?endif?>
<p<?if(CSite::InDir($arSection["SECTION_PAGE_URL"])):?> class="selected"<?endif?>><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a> <?if($arSection["ELEMENT_CNT"]>0):?><span>[<?=$arSection["ELEMENT_CNT"]?>]</span><?endif;?></p>
<?$counter++;
endforeach;?>
<?if($key > 10):?></div><?endif?>
</div>		
<div id="left_more" class="left_more"><p><a href="javascript:void(0)">Показать еще</a></p></div>	
</div> 
<?endif;?>