<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

foreach ($arResult["arrProp"] as $key => $value)
{
	$arResult["arrProp"][$key]["SELECT_VALUE"] = $_GET["arrFilter_pf"][$value["CODE"]];
	if ($value["CODE"] == "TYPE")
	{
		$arResult["arrProp"][$key]["NAME"] = "Tипу";
	}
	if ($value["CODE"] == "SIZE")
	{
		$arResult["arrProp"][$key]["NAME"] = "Размер";
		
	}
	if ($value["CODE"] == "SIZE2")
	{
		$arFilter = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y"
			);
		if ($arResult["SECTION_CODE"]) $arFilter["SECTION_CODE"] = $arResult["SECTION_CODE"];
		$arSelect = array("PROPERTY_" . $value["CODE"]);
		$arResult["arrProp"][$key]["NAME"] = "Размер";
		$produsersObj = CIBlockElement::GetList(
			array("SORT"=>"ASC"),
			$arFilter,
			$arSelect
		);
		while ($produser = $produsersObj->GetNext())
		{
			$arResult["arrProp"][$key]["PROPERTY_TYPE"] = "L";
			if ($produser["PROPERTY_".$value["CODE"]."_VALUE"])
			{
				$element = CIBlockElement::GetByID($produser["PROPERTY_".$value["CODE"]."_VALUE"])->GetNext();
				$arResult["arrProp"][$key]["VALUE_LIST"][$produser["PROPERTY_".$value["CODE"]."_VALUE"]] = $element["NAME"];
			}
		}
		asort($arResult["arrProp"][$key]["VALUE_LIST"]);
		
	}
	if ($value["CODE"] == "PRODUCER")
	{
		$arFilter = array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y"
			);
		if ($arResult["SECTION_CODE"]) $arFilter["SECTION_CODE"] = $arResult["SECTION_CODE"];
		$arSelect = array("PROPERTY_" . $value["CODE"]);
		$arResult["arrProp"][$key]["NAME"] = "Производитель";
		$produsersObj = CIBlockElement::GetList(
			array("SORT"=>"ASC"),
			$arFilter,
			$arSelect
		);
		while ($produser = $produsersObj->GetNext())
		{
			$arResult["arrProp"][$key]["PROPERTY_TYPE"] = "L";
			if ($produser["PROPERTY_PRODUCER_VALUE"])
			{
				$element = CIBlockElement::GetByID($produser["PROPERTY_PRODUCER_VALUE"])->GetNext();
				$arResult["arrProp"][$key]["VALUE_LIST"][$produser["PROPERTY_PRODUCER_VALUE"]] = $element["NAME"];
			}
		}
	}
}
$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"IBLOCK_SECTION_ID" => false
	);

$sectionsObj = CIBlockSection::GetList(
		array("SORT"=>"ASC"),
		$arFilter
	);
$sections = array(
	"CODE" => "SECTION",
	"NAME" => "Сюжет",
	"VALUE_LIST" => array()
);

$dir = trim($APPLICATION->GetCurDir(),"/");
$arDir = explode("/", $dir);
$sections["SELECT_VALUE"] = $arDir[1];

while ($section = $sectionsObj->GetNext())
{
	$sections["VALUE_LIST"][$section["CODE"]] = $section["NAME"];
}

array_unshift($arResult["arrProp"], $sections);

?>
