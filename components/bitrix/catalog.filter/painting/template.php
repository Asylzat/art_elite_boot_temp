<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script type="text/javascript">
function getAction(curent)
{
	if ($(curent).attr('value') == "")
	{
		return "/catalog/";
	}
	else
	{
		return '/catalog/' + $(curent).attr('value') + '/';
	}
}
</script>
<form id="filter" name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="" method="get">
<?foreach($arResult["ITEMS"] as $arItem):
	if(array_key_exists("HIDDEN", $arItem)):
		echo $arItem["INPUT"];
	endif;
endforeach;?>
<?foreach($arResult["arrProp"] as $arItem):?>
<div class="section">
	<?if ($arItem["CODE"] == "SECTION"):?>
	<select onchange="$('#filter').attr('action', getAction(this))">
	<?else:?>
	<select name="arrFilter_pf[<?=$arItem["CODE"]?>]">
	<?endif;?>
	<option value="">Все <?=strtolower($arItem["NAME"])?></option>
	<?foreach($arItem["VALUE_LIST"] as $key=>$element):?>
	<?if ($arItem["SELECT_VALUE"] == $key):?>
	<option value="<?=$key?>" selected><?=$element?></option>
	<?else:?>
	<option value="<?=$key?>"><?=$element?></option>
	<?endif;?>
	<?endforeach?>
	</select>
</div>
<?endforeach;?>
<input type="hidden" name="set_filter" value="Y" />
<div class="top_filtr_button"><a href="javascript:void(0)" onclick="$('#filter').submit()">Найти</a></div>	
</form>
