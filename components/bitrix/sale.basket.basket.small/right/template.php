<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$allCurrency = CSaleLang::GetLangCurrency(SITE_ID);
$arResult["ITEM_COUNTER"] = Array(
        "READY" => Array("C" => 0, "p" => 0),
        "DELAY" => Array("C" => 0, "p" => 0),
        "NOTAVAIL" => Array("C" => 0, "p" => 0),                
);
if (is_array($arResult["ITEMS"]))
{
	foreach ($arResult["ITEMS"] as $v)
	{
		if ($v["DELAY"] == "N" && $v["CAN_BUY"] == "Y")
		{
			   $arResult["ITEM_COUNTER"]["READY"]["C"] += $v["QUANTITY"];
			   $arResult["ITEM_COUNTER"]["READY"]["P"] += ($v["QUANTITY"] * $v["PRICE"]);
		}
		elseif ($v["DELAY"]=="Y" && $v["CAN_BUY"]=="Y")
		{
			   $arResult["ITEM_COUNTER"]["DELAY"]["C"] += $v["QUANTITY"];
			   $arResult["ITEM_COUNTER"]["DELAY"]["P"] += ($v["QUANTITY"] * $v["PRICE"]);
		}
		if ($v["CAN_BUY"] == "N")
		{
			   $arResult["ITEM_COUNTER"]["NOTAVAIL"]["C"] += $v["QUANTITY"];
			   $arResult["ITEM_COUNTER"]["NOTAVAIL"]["P"] += ($v["QUANTITY"] * $v["PRICE"]);
		}	
	}
}
//p($arResult);
?>
<?$product = "товар";
if (($arResult["ITEM_COUNTER"]["READY"]["C"] > 1) && ($arResult["ITEM_COUNTER"]["READY"]["C"] < 5)) $product = "товара";
if ($arResult["ITEM_COUNTER"]["READY"]["C"] > 4) $product = "товаров";

?>
<?if ($arResult["READY"]=="Y"):?>
        <?if ($arResult["READY"]=="Y"):?>
				<div class="top_basket">
					<p>В Вашей корзине:</p>
					<p><a href="<?=$arParams["PATH_TO_BASKET"]?>"><?=$arResult["ITEM_COUNTER"]["READY"]["C"]?> <?=$product?></a> на сумму <a href="<?=$arParams["PATH_TO_BASKET"]?>"><?=SaleFormatCurrency($arResult["ITEM_COUNTER"]["READY"]["P"], "RUB")?></a></p>
					<p><a class="button" href="<?=$arParams["PATH_TO_BASKET"]?>">Оформить заказ</a></p>
				</div>           
        <?endif?>
<?else:?>
<div class="top_basket no_element">
		<p>В Вашей корзине пока нет товаров</p>
		<p><a title="Мы принимаем: MasterCard, Visa, наличными." href="/delivery/"><img src="<?=SITE_TEMPLATE_PATH?>/img/top_pay.png" width="229" height="37" alt="Способы оплаты" /></a></p>

</div>
<?endif;?>