<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
foreach($arResult as $id => $arItem)
{
	if(!array_key_exists("PICTURE", $arItem))
	{
		$rsMorePhoto = CIBlockElement::GetProperty($arParams["IBLOCK_ID"], $id, array(), array("CODE" => "MORE_PHOTO", "EMPTY" => "N"));
		if ($morePhoto = $rsMorePhoto->GetNext())
		{
			$arResult[$id]["PICTURE"] = ImgFileResizeFactory::ResizeImgById($morePhoto["VALUE"], 50, 50, "", ImgFileResizeFactory::CENTER_CROP, true);
			$_SESSION[$arParams["NAME"]][$arParams["IBLOCK_ID"]]["ITEMS"][$id] = $arResult[$id];
		}
	}
}
?>