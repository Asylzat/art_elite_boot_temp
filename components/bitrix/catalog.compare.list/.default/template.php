<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult)>0):?>
<div class="left_compare">
<div class="head"><p>Сравнение</p></div>		
<div class="left_block_compare">
<?foreach($arResult as $id=>$arElement):?>
	<p><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$arElement["PICTURE"]["SRC"]?>" alt="" width="<?=$arElement["PICTURE"]["WIDTH"]?>" height="<?=$arElement["PICTURE"]["HEIGHT"]?>" /><span><?=$arElement["NAME"]?></span></a><a class="del" href="javascript:void(0)" onclick="unCheckCompare([<?=$id?>]);SendCompare('DELETE_FROM_COMPARE_LIST', <?=$id?>);inCompare--;"></a></p>
<?endforeach?>
<?if(count($arResult)>=2):?>
	<a class="main_by" href="<?=$arParams["COMPARE_URL"]?>"><?=GetMessage("CATALOG_COMPARE")?></a>
<?endif;?>

</div>
</div>
<?endif;?>
