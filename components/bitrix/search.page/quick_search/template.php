<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
if(count($arResult["SEARCH"]) > 0):?>
<div class="quick_search">
<?foreach($arResult["SEARCH"] as $key=>$arItem):?>
<?if ($key%3 == 0):?>
<div class="quick_search_tab" <?if ($key > 0):?>style="display:none"<?endif?>>
<?endif;?>
<div>
	<div><a href="<?=$arItem["URL_WO_PARAMS"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" /></a></div>
	<div>
		<p><a href="<?=$arItem["URL_WO_PARAMS"]?>"><?=$arItem["TITLE"]?></a></p>
		<p>Размер: <?=$arItem["SIZE"]["VALUE"]?> см.</p>
		<?if($arItem["COMPLEXITY"]["VALUE"]):?>
		<p>Сложность: <em class="rating_<?=$arItem["COMPLEXITY"]["VALUE"]?>"></em></p>
		<?endif;?>
		<p>Тип: <?=$arItem["TYPE"]["VALUE"]?></p>
		<p class="main_price">Цена: <span><?=$arItem["PRICE"]?>.</span></p>	
		<?if($arItem["CAN_BUY"] == "Y"):?>
		<p><a class="main_by quickSearchAddToCart" href="<?=$arItem["URL_WO_PARAMS"]?>?action=ADD2BASKET&id=<?=$arItem["ITEM_ID"]?>">В корзину</a></p>
		<?endif;?>
	</div>
</div>
<?if (($key+1)%3 == 0 && $key > 0):?>
</div>
<?endif;?> 
<?endforeach;
if (($key+1)%3 != 0 && $key > 0):?>
</div>
<?endif;
if (count($arResult["SEARCH"]) > 3):?>
<p class="more"><a data-value="0" href="">Показать ещё</a> &rarr;</p>
<?endif;?>
</div>
<?endif;?>
<script>
$('.quick_search .more a').bind('click', function() {
	var countTab = $('.quick_search .quick_search_tab').length;
	var curTab = $(this).data('value');
	if (curTab + 1 < countTab)
	{
		$('.quick_search .quick_search_tab').eq(curTab).fadeOut(150, function(){
			$('.quick_search .quick_search_tab').eq(curTab+1).fadeIn(150);
			$('.quick_search .more a').data('value', curTab+1);
		});	
	}
	else
	{
		$('.quick_search .quick_search_tab').eq(curTab).fadeOut(150, function(){
			$('.quick_search .quick_search_tab').eq(0).fadeIn(150);
			$('.quick_search .more a').data('value', 0);
		});	
	}
	return false;
});
</script>