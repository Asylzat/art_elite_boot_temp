<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
foreach($arResult["SEARCH"] as $key=>$arItem)
{
	if ($elementObj = CIBlockElement::GetByID($arItem["ITEM_ID"])->GetNextElement())
	{
		$elementCatObj = CCatalogProduct::GetList(
			array("QUANTITY" => "DESC"),
			array("ID" => $arItem["ITEM_ID"]),
			false,
			array("nTopCount" => 1)
		);
		if ($elementCat = $elementCatObj->Fetch())
		{
			$arResult["SEARCH"][$key]["CAN_BUY"] = "N";
			if ($elementCat["QUANTITY"] > 0) $arResult["SEARCH"][$key]["CAN_BUY"] = "Y";
		}
		$photo = $elementObj->GetProperty("MORE_PHOTO");
		if (intVal($photo["VALUE"][0]) > 0)
		{
			$img = CFile::ResizeImageGet( 
				$photo["VALUE"][0], 
				array(width=>"110", height=>"110"), 
				BX_RESIZE_IMAGE_EXACT, 
				true
			);
			if (!empty($img)) 
				$arResult["SEARCH"][$key]["PREVIEW_PICTURE"] = array_change_key_case($img, CASE_UPPER);
		}
		$arResult["SEARCH"][$key]["SIZE"] = $elementObj->GetProperty("SIZE");
		$arResult["SEARCH"][$key]["COMPLEXITY"] = $elementObj->GetProperty("COMPLEXITY");
		$arResult["SEARCH"][$key]["TYPE"] = $elementObj->GetProperty("TYPE");
		$price = CPrice::GetBasePrice($arItem["ITEM_ID"]);
		$price2 = CatalogGetPriceTableEx($arItem["ITEM_ID"], 0, array(1), 'Y');
		if(!empty($price2["MATRIX"][1][0]["DISCOUNT_PRICE"]))
			$price["PRICE"] = $price2["MATRIX"][1][0]["DISCOUNT_PRICE"];
		$arResult["SEARCH"][$key]["PRICE"] = SaleFormatCurrency($price["PRICE"], "RUB");
		//$arResult["SEARCH"][$key]["PRICE_2"] = $price2["MATRIX"];
	}
}
?>
