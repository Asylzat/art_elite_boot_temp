<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N"):
if(empty($_REQUEST["ACTIVE_STEP"])) $_REQUEST["ACTIVE_STEP"] = 1;
?>
<div class="basket_step">
  
    <div class="step_name"><span>ШАГ <?=$step++?></span></div>
   	 
    <div class="step_text"> 		 
      <p>Укажите, вы новый покупатель или уже зарегистрированный?</p>
     		 
      <p><em>Для оформления заказа необходимо заполнить все обязательные поля</em></p>
     	</div>
   	 
    <div class="step_sort"> 
      <p <?if($_REQUEST["do_authorize"] != "Y"):?>class="selected_new"<?endif;?>><a id="select_input_new" href="javascript:void(0)" >Новый</a><a id="select_input_registration" class="selected" href="javascript:void(0)" >Уже зарегистрированный</a></p>
     </div>
   
<script type="text/javascript">
$('#select_input_new').bind('click', function() {
	$(this).addClass('selected').parent().addClass('selected_new');
	$('#select_input_registration').removeClass('selected');
	
	$('#reg_form').show();
	$('#auth_form').hide();
});
$('#select_input_registration').bind('click', function() {
	$(this).addClass('selected').parent().removeClass('selected_new');
	$('#select_input_new').removeClass('selected');
	$('#auth_form').show();
	$('#reg_form').hide();
});
</script>
 </div>
<div class="basket_step selected_tab">
	<div class="step_name"><span>ШАГ <?=$step++?></span></div>
	<span id="auth_form"  <?if($_REQUEST["do_authorize"] != "Y"):?>style="display:none"<?endif;?>>
	<div class="step_text">
		<p>Введите свои данные <span class="error">*</span></p>
		<p><em>Введите логин/email и пароль, и введенные вами ранее данные автоматически заполнятся</em></p>
		<?if($_REQUEST["do_authorize"] == "Y"):
		foreach($arResult["ERROR"] as $error):?>
		<p class="error_auth"><?=$error?></p>
		<?endforeach;
		endif;
		?>
	</div>
	<div class="step_form">	
		<form method="post" action="" id="order_auth_form" name="order_auth_form">
			<input type="hidden" name="do_authorize" value="Y">
			<?=bitrix_sessid_post()?>
			<?
			foreach ($arResult["POST"] as $key => $value)
			{
			?>
			<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?
			}
			?>
			<div><span>Логин/Email:<em class="error">*</em></span> <input type="text" name="USER_LOGIN" value="<?=$arResult["AUTH"]["USER_LOGIN"]?>" maxlength="35" /></div>
			<div><span>Пароль:<em class="error">*</em></span> <input type="password" name="USER_PASSWORD" maxlength="35" /></div>
			<div><a href="<?=$arParams["PATH_TO_AUTH"]?>?forgot_password=yes&back_url=<?= urlencode($APPLICATION->GetCurPageParam()); ?>" class="step_remind">Забыли пароль?</a></div>
		</form>
		<p><a href="javascript:voide(0)" class="button" onclick="$('#order_auth_form').submit()">Перейти к шагу <?=$step?></a></p>	
	</div>
	</span>
	<span id="reg_form" <?if($_REQUEST["do_authorize"] == "Y"):?>style="display:none"<?endif;?>>
	<div class="step_text">
		<p>Введите свои данные <span class="error">*</span></p>
		<p><em>Личные данные</em></p>
		<?if($_REQUEST["do_authorize"] != "Y"):
		foreach($arResult["ERROR"] as $error):?>
		<p class="error_auth"><?=$error?></p>
		<?endforeach;
		endif;
		?>
	</div>
	<div class="step_form">	
		<form method="post" action="" id="order_reg_form" name="order_reg_form">
			<?=bitrix_sessid_post()?>
			<?
			foreach ($arResult["POST"] as $key => $value)
			{
			?>
			<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?
			}
			?>
			<div><span>Имя:<em class="error">*</em></span> <input name="NEW_NAME" type="text" value="<?=$arResult["AUTH"]["NEW_NAME"]?>" maxlength="35" /></div>
			<div><span> Фамилия:<em class="error">*</em></span> <input name="NEW_LAST_NAME" type="text" value="<?=$arResult["AUTH"]["NEW_LAST_NAME"]?>" maxlength="35" /></div>
			<div><span>E-Mail:<em class="error">*</em></span> <input name="NEW_EMAIL" type="text" value="<?=$arResult["AUTH"]["NEW_EMAIL"]?>" maxlength="35" /></div>
			<input type="hidden" name="NEW_GENERATE" value="Y">
			<?
			if($arResult["AUTH"]["captcha_registration"] == "Y") //CAPTCHA
			{
				?>
				<input type="hidden" name="captcha_sid" value="<?=$arResult["AUTH"]["capCode"]?>">		
				<div><span>Введите код:<em class="error">*</em></span> <input type="text" name="captcha_word" size="30" maxlength="35" value=""><img class="captcha_img" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["AUTH"]["capCode"]?>" width="180" height="40" alt="CAPTCHA"></div>
				<?
			}
			?>
			<input type="hidden" name="do_register" value="Y">
		</form>
		<p><a href="javascript:voide(0)" class="button" onclick="$('#order_reg_form').submit()">Перейти к шагу <?=$step?></a></p>	
	</div>
	</span>

</div>
<?else:?>
<div class="next_tab"><span>ШАГ <?=$step++?> Выбор способа авторизации</span></div>
<div class="next_tab"><span>ШАГ <?=$step++?> авторизация / регистрация</span></div>
<?
if(empty($_REQUEST["ACTIVE_STEP"])) $_REQUEST["ACTIVE_STEP"] = 2;
endif?>
