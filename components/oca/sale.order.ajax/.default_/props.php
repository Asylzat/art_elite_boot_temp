<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$styleNextTab = ' style="display:none"';
$styleBasketStep = ' style="display:block"';
if ($_REQUEST["ACTIVE_STEP"] <> $step)
{
	$styleNextTab = ' style="display:block"';
	$styleBasketStep = ' style="display:none"';
}
?>
<?
function PrintPropsForm($arSource=Array(), $locationTemplate = ".default")
{
	if (!empty($arSource))
	{
		foreach($arSource as $arProperties):
		$required = '';
		?>
			<div><span><?=$arProperties["NAME"]?>:<?if($arProperties["REQUIED_FORMATED"]=="Y"): $required = ' class="required_field"';?><em class="error">*</em><?endif;?></span> 
				<?if($arProperties["TYPE"] == "CHECKBOX"):
					?><input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" value=""><input type="checkbox" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" value="Y"<?if ($arProperties["CHECKED"]=="Y") echo " checked";?>><?
				elseif($arProperties["TYPE"] == "TEXT"):
					?><input<?=$required?> type="text" maxlength="35" value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>"><?
				elseif($arProperties["TYPE"] == "SELECT"):
					?><select<?=$required?> name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" maxlength="35"><?
						foreach($arProperties["VARIANTS"] as $arVariants):
							?><option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option><?
						endforeach;
					?></select><?
				elseif ($arProperties["TYPE"] == "MULTISELECT"):
					?><select multiple name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" maxlength="35"><?
						foreach($arProperties["VARIANTS"] as $arVariants):
							?><option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option><?
						endforeach;
					?></select><?
				elseif ($arProperties["TYPE"] == "TEXTAREA"):
					?><textarea<?=$required?> rows="<?=$arProperties["SIZE2"]?>" cols="<?=$arProperties["SIZE1"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>"><?=$arProperties["VALUE"]?></textarea><?
				elseif ($arProperties["TYPE"] == "LOCATION"):
						$value = 1;
						if (empty($_REQUEST[$arProperties["FIELD_NAME"]]))
							$value = 1;
						if (is_array($arProperties["VARIANTS"]) && count($arProperties["VARIANTS"]) > 0)
						{
							foreach ($arProperties["VARIANTS"] as $arVariant)
							{
								if ($arVariant["SELECTED"] == "Y")
								{
									$value = $arVariant["ID"];
									break;
								}
							}
						}
						$GLOBALS["APPLICATION"]->IncludeComponent(
							"bitrix:sale.ajax.locations",
							$locationTemplate,
							array(
								"AJAX_CALL" => "N",
								"COUNTRY_INPUT_NAME" => "COUNTRY",//.$arProperties["FIELD_NAME"],
								"REGION_INPUT_NAME" => "REGION",//.$arProperties["FIELD_NAME"],
								"CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
								"CITY_OUT_LOCATION" => "Y",
								"LOCATION_VALUE" => $value,
								"ORDER_PROPS_ID" => $arProperties["ID"],
								"ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
								"SIZE1" => $arProperties["SIZE1"],
							),
							null,
							array('HIDE_ICONS' => 'Y')
						);
				elseif ($arProperties["TYPE"] == "RADIO"):
					foreach($arProperties["VARIANTS"] as $arVariants):
						?><input type="radio" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>" value="<?=$arVariants["VALUE"]?>"<?if($arVariants["CHECKED"] == "Y") echo " checked";?>> <label for="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"><?=$arVariants["NAME"]?></label><br/><?
					endforeach;
				endif;
					if (strlen($arProperties["DESCRIPTION"]) > 0)
					{
						?><br /><small><?echo $arProperties["DESCRIPTION"] ?></small><?
					}
				
				?></div><?
		endforeach;
		return true;
	}
	return false;
}
?>
<div class="next_tab" id="tab_name_<?=$step?>"<?=$styleNextTab?>><span>ШАГ <?=$step?> <?=GetMessage("SOA_TEMPL_PROP_INFO")?></span><a href="javascript:void(0)" class="link" onclick="clickStep(<?=$step?>)" <?if (intVal($_REQUEST["ACTIVE_STEP"])<$step):?>style="display:none"<?endif;?>>Развернуть</a></div>
<div class="basket_step selected_tab" id="tab_content_<?=$step?>"<?=$styleBasketStep?>>
	<div class="step_name"><span>ШАГ <?=$step++?></span></div>
	<div class="step_text">
		<p><?=GetMessage("SOA_TEMPL_PROP_INFO")?> <span class="error">*</span></p>
		<p><em>Для того, чтобы мы знали, куда доставить ваш заказ</em></p>
		<?
		if(!empty($arResult["ORDER_PROP"]["USER_PROFILES"]))
		{
			?><p>
			<?=GetMessage("SOA_TEMPL_PROP_CHOOSE")?><br />
			<select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="SetContact(this.value)">
				<option value="0"><?=GetMessage("SOA_TEMPL_PROP_NEW_PROFILE")?></option>
				<?
				foreach($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles)
				{
					?>
					<option value="<?= $arUserProfiles["ID"] ?>"<?if ($arUserProfiles["CHECKED"]=="Y") echo " selected";?>><?=$arUserProfiles["NAME"]?></option>
					<?
				}
				?>
			</select>
			</p>
			<?
		}

		?>
	</div>

<div style="display:none;">
<?
	$APPLICATION->IncludeComponent(
		"bitrix:sale.ajax.locations",
		$arParams["TEMPLATE_LOCATION"],
		array(
			"AJAX_CALL" => "N",
			"COUNTRY_INPUT_NAME" => "COUNTRY_tmp",
			"REGION_INPUT_NAME" => "REGION_tmp",
			"CITY_INPUT_NAME" => "tmp",
			"CITY_OUT_LOCATION" => "Y",
			"LOCATION_VALUE" => "1",
			"ONCITYCHANGE" => "submitForm()",
		),
		null,
		array('HIDE_ICONS' => 'Y')
	);
?>
</div>

	<div class="step_form">	
<?
PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_N"], $arParams["TEMPLATE_LOCATION"]);
PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"]);
?>
<p><a href="javascript:void(0)" class="button" onclick="NextStep(<?=$step?>)">Перейти к шагу <?=$step?></a></p>
	</div>
</div>
<script>
//$("#ORDER_PROP_3").inputmask("+7(999) 999-99-99", { "clearIncomplete": true});
</script>
