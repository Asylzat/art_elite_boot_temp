<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$styleNextTab = ' style="display:none"';
$styleBasketStep = ' style="display:block"';
if ($_REQUEST["ACTIVE_STEP"] <> $step)
{
	$styleNextTab = ' style="display:block"';
	$styleBasketStep = ' style="display:none"';
}
?>
<div class="next_tab" id="tab_name_<?=$step?>"<?=$styleNextTab?>><span>ШАГ <?=$step?> <?=GetMessage("SOA_TEMPL_DELIVERY")?></span><a href="javascript:void(0)" class="link" onclick="clickStep(<?=$step?>)" <?if (intVal($_REQUEST["ACTIVE_STEP"])<$step):?>style="display:none"<?endif;?>>Развернуть</a></div>
<?
if(!empty($arResult["DELIVERY"]))
{
	?>
<div class="basket_step selected_tab" id="tab_content_<?=$step?>"<?=$styleBasketStep?>>
	<div class="step_name"><span>ШАГ <?=$step++?></span></div>
	<div class="step_text">
		<p><?=GetMessage("SOA_TEMPL_DELIVERY")?><span class="error">*</span></p>
	</div>
	<div class="step_form">
		<?
		$actualDelivery = array();
		foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery)
		{
			if ($delivery_id !== 0 && intval($delivery_id) <= 0)
			{
				?>
				<tr>
					<td colspan="2">
						<b><?=$arDelivery["TITLE"]?></b><?if (strlen($arDelivery["DESCRIPTION"]) > 0):?><br />
						<?=nl2br($arDelivery["DESCRIPTION"])?><br /><?endif;?>
						<table border="0" cellspacing="0" cellpadding="3">
						<?
						foreach ($arDelivery["PROFILES"] as $profile_id => $arProfile)
						{
							?>
							<tr>
								<td width="0%" valign="top">
									<input type="radio" id="ID_DELIVERY_<?=$delivery_id?>_<?=$profile_id?>" name="<?=$arProfile["FIELD_NAME"]?>" value="<?=$delivery_id.":".$profile_id;?>" <?=$arProfile["CHECKED"] == "Y" ? "checked=\"checked\"" : "";?> onclick="submitForm();" />
								</td>
								<td width="50%" valign="top">
									<label for="ID_DELIVERY_<?=$delivery_id?>_<?=$profile_id?>">
										<small><b><?=$arProfile["TITLE"]?></b><?if (strlen($arProfile["DESCRIPTION"]) > 0):?><br />
										<?=nl2br($arProfile["DESCRIPTION"])?><?endif;?></small>
									</label>
								</td>
								<td width="50%" valign="top" align="right">
								<?
									$APPLICATION->IncludeComponent('bitrix:sale.ajax.delivery.calculator', '', array(
										"NO_AJAX" => $arParams["DELIVERY_NO_AJAX"],
										"DELIVERY" => $delivery_id,
										"PROFILE" => $profile_id,
										"ORDER_WEIGHT" => $arResult["ORDER_WEIGHT"],
										"ORDER_PRICE" => $arResult["ORDER_PRICE"],
										"LOCATION_TO" => $arResult["USER_VALS"]["DELIVERY_LOCATION"],
										"LOCATION_ZIP" => $arResult["USER_VALS"]["DELIVERY_LOCATION_ZIP"],
										"CURRENCY" => $arResult["BASE_LANG_CURRENCY"],
									), null, array('HIDE_ICONS' => 'Y'));
								?>

								</td>
							</tr>
							<?
						} // endforeach
						?>
						</table>
					</td>
				</tr>
				<?
			}
			else
			{
				if ($arDelivery["CHECKED"]=="Y") $actualDelivery = $arDelivery;
				?>
				<div><span><label for="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"><?= $arDelivery["NAME"] ?></label></span><input class="courier" type="radio" id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>" name="<?=$arDelivery["FIELD_NAME"]?>" value="<?= $arDelivery["ID"] ?>"<?if ($arDelivery["CHECKED"]=="Y") echo " checked";?> onclick="submitForm();" /></div>
				<?
			}
		}
		?>
	<p><a href="javascript:void(0)" class="button" onclick="NextStep(<?=$step?>)">Перейти к шагу <?=$step?></a></p>	
	</div>
	<div class="step_order">
	<?if (count($actualDelivery) > 0):?>
		<p>Информация о доставке:</p>
		<ul>
			<?if (strlen($actualDelivery["PERIOD_TEXT"])>0):?>
			<li><span><?=$actualDelivery["PERIOD_TEXT"];?></span></li>
			<?endif;?>
			<li><span><?=GetMessage("SALE_DELIV_PRICE");?> <?=$actualDelivery["PRICE_FORMATED"]?></span></li>
			<?if (strlen($actualDelivery["DESCRIPTION"])>0):?>
			<li><span><?=$actualDelivery["DESCRIPTION"];?></span></li>
			<?endif;?>
		</ul>
		<?endif;?>
	</div>
</div>
	<?
}
else
{
$step++;
}
?>