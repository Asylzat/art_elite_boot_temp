<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (intVal($arResult["ORDER_ID"]) > 0)
{
	$dbBasketItems = CSaleBasket::GetList(
		array(
				"PRICE" => "ASC"
			),
		array(
				"ORDER_ID" => $arResult["ORDER_ID"]
			),
		false,
		false,
		array(
			"PRICE", "WEIGHT", "QUANTITY", "NAME", "DETAIL_PAGE_URL"
		)
	);
	$quantity = 0;
	//$price = 0;
	$elements = array();
	while ($arItem = $dbBasketItems->Fetch())
	{
		//$quantity += $arItem["QUANTITY"];
		//$price += $arItem["PRICE"]*$arItem["QUANTITY"];
		$arResult["ORDER"]["ITEMS"][] = $arItem;
	}
	
}
?>