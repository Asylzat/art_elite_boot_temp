<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$step=0;
?>
<?if(!$arResult["ORDER_ID"]):?>
<h1>Оформление заказа</h1>
<?else:
$paymentType = trim(CSalePaySystemAction::GetParamValue("PAYMENT_VALUE"));
?>
	<?if (strlen($paymentType) > 0 && $paymentType != "0" && $arResult["ORDER"]["PAYED"] != "Y"):?>
	<h1>Ваш заказ принят в обработку и ожидает оплаты</h1>	
	<?elseif($arResult["ORDER"]["PAYED"] == "Y"):?>
	<h1>Ваш заказ оплачен и принят в обработку</h1>
	<?else:?>
	<h1>Ваш заказ принят в обработку</h1>
	<?endif;?>
<?endif;?>


<script>
function clickStep(number)
{
	var i=number;
	while(i < 6)
	{
		$('#tab_content_'+i).slideUp("slow");
		$('#tab_name_'+i).slideDown("slow").find('.link').hide();
		i++;
	}
	$('#tab_name_' + number).slideUp("slow", function() {
		$('#tab_content_' + number).slideDown("slow");
		BX("active_step").value = number;
	}).find('.link').show();
}

function NextStep(number)
{
	result = true;
	if (number-1 == 2)
	{
		$('#tab_content_' + (number-1) + ' .required_field').each(function(){
			if ($(this).val().trim() == '')
			{
				result = false;
				$(this).addClass("error_input");
			}
			else
			{
				$(this).removeClass("error_input");
			}
		});
	}
	if (!result) return;
	$('#tab_content_' + (number-1)).slideToggle( "slow", function() {
		$('#tab_name_' + (number-1)).slideToggle("slow", function(){
			$('#tab_name_' + number).slideToggle("slow", function() {
				$('#tab_content_' + number).slideToggle("slow");
			});
		}).find('.link').show();
	});

	BX("active_step").value = number;
}
</script>

<a name="order_fform"></a>
<div id="order_form_div" class="basket">                                                                                                                                       
<NOSCRIPT>
	<div class="errortext"><?=GetMessage("SOA_NO_JS")?></div>
</NOSCRIPT>
<?

	if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
	{
		if(strlen($arResult["REDIRECT_URL"]) > 0)
		{
			?>
			<script type="text/javascript">
			window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
			</script>
			<?
			die();
		}
		else
		{
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
		}
	}
	else
	{
		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
		?>
		
		<script type="text/javascript">
		function submitFormClick()
		{
			$("#submitbutton").click();
			return false;
		}
		function submitForm(val)
		{
			if(val != 'Y')
				BX('confirmorder').value = 'N';

			var orderForm = BX('ORDER_FORM');
			BX.ajax.submitComponentForm(orderForm, 'order_form_content', true);
			BX.submit(orderForm);
			return true;
		}
		function SetContact(profileId)
		{
			BX("profile_change").value = "Y";
			submitForm();
		}
		</script>
		<?if($_POST["is_ajax_post"] != "Y")
		{
			?><form action="" method="POST" name="ORDER_FORM" id="ORDER_FORM">
			<?=bitrix_sessid_post();?>
			<div id="order_form_content">
			<?
		}
		else
		{
			$APPLICATION->RestartBuffer();
		}
		//if(empty($_REQUEST["ACTIVE_STEP"])) $_REQUEST["ACTIVE_STEP"] = 2;
		if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
		{
			foreach($arResult["ERROR"] as $v)
				echo ShowError($v);

			?>
			<script type="text/javascript">
				//top.BX.scrollToNode(top.BX('ORDER_FORM'));
			</script>
			<?
		}
		if(count($arResult["PERSON_TYPE"]) > 1)
		{
			?>
			<table class="sale_order_full_table" style="display:none">
			<tr>
			<td>
			<?
			foreach($arResult["PERSON_TYPE"] as $v)
			{
				?><input type="radio" id="PERSON_TYPE_<?= $v["ID"] ?>" name="PERSON_TYPE" value="<?= $v["ID"] ?>"<?if ($v["CHECKED"]=="Y") echo " checked=\"checked\"";?> onClick="submitForm()"> <label for="PERSON_TYPE_<?= $v["ID"] ?>"><?= $v["NAME"] ?></label><br /><?
			}
			?>
			<input type="hidden" name="PERSON_TYPE_OLD" value="<?=$arResult["USER_VALS"]["PERSON_TYPE_ID"]?>">
			</td></tr></table>
			<?
		}
		else
		{
			if(IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) > 0)
			{
				?>
				<input type="hidden" name="PERSON_TYPE" value="<?=IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"])?>">
				<input type="hidden" name="PERSON_TYPE_OLD" value="<?=IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"])?>">
				<?
			}
			else
			{
				foreach($arResult["PERSON_TYPE"] as $v)
				{
					?>
					<input type="hidden" id="PERSON_TYPE" name="PERSON_TYPE" value="<?=$v["ID"]?>">
					<input type="hidden" name="PERSON_TYPE_OLD" value="<?=$v["ID"]?>">
					<?
				}
			}
		}

		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");
		?>
		<?
		if ($arParams["DELIVERY_TO_PAYSYSTEM"] == "p2d")
		{
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
		}
		else
		{
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
		}
		?>
		<?
		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");

		if(strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
			echo $arResult["PREPAY_ADIT_FIELDS"];
		?>
		<?if($_POST["is_ajax_post"] != "Y")
		{
			?>
				</div>
				<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
				<input type="hidden" name="profile_change" id="profile_change" value="N">
				<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
			<input type="hidden" name="ACTIVE_STEP" id="active_step" value="<?=$_REQUEST["ACTIVE_STEP"]?>">
			<div align="right" style="display:none">
				<input type="button" id="submitbutton" name="submitbutton" onClick="submitForm('Y');" value="<?=GetMessage("SOA_TEMPL_BUTTON")?>">
			</div>
			<?if($_POST["is_ajax_post"] != "Y")
			{
				?><script>submitForm();</script><?
			}
			?>
			</form>
			<?
			if($arParams["DELIVERY_NO_AJAX"] == "N")
			{
				$APPLICATION->AddHeadScript("/bitrix/js/main/cphttprequest.js");
				$APPLICATION->AddHeadScript("/bitrix/components/bitrix/sale.ajax.delivery.calculator/templates/.default/proceed.js");
			}
		}
		else
		{
			?>
			<script type="text/javascript">
				top.BX('confirmorder').value = 'Y';
				top.BX('profile_change').value = 'N';
			</script>
			<?
			die();
		}
	}

?>
</div>