<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$styleNextTab = ' style="display:none"';
$styleBasketStep = ' style="display:block"';
if ($_REQUEST["ACTIVE_STEP"] <> $step)
{
	$styleNextTab = ' style="display:block"';
	$styleBasketStep = ' style="display:none"';
}
?>
<div class="next_tab" id="tab_name_<?=$step?>"<?=$styleNextTab?>><span>ШАГ <?=$step?> <?=GetMessage("SOA_TEMPL_SUM_TITLE")?></span><a href="javascript:void(0)" class="link" onclick="clickStep(<?=$step?>)" <?if (intVal($_REQUEST["ACTIVE_STEP"])<$step):?>style="display:none"<?endif;?>>Развернуть</a></div>
<div class="basket_step selected_tab" id="tab_content_<?=$step?>"<?=$styleBasketStep?>>
	<div class="step_name"><span>ШАГ <?=$step++?></span></div>
	<div class="step_text sostav">
		<p><?=GetMessage("SOA_TEMPL_SUM_TITLE")?>:</p>

<table>
	<tr>
		<th>Наименование</th>
		<th>Количество</th>
		<th>Цена</th>
		<th>За доставку</th>
		<th>Общая стоимость</th>
	</tr>
	<?
	foreach($arResult["BASKET_ITEMS"] as $keyBasket=>$arBasketItems)
	{
		?>
		<tr>
			<td><?=$arBasketItems["NAME"]?></td>
			<td><?=$arBasketItems["QUANTITY"]?></td>
			<td align="right"><?=$arBasketItems["PRICE_FORMATED"]?></td>
			<?if ($keyBasket < 1):?>
			<td rowspan="<?=count($arResult["BASKET_ITEMS"])?>"><?=$arResult["DELIVERY_PRICE_FORMATED"]?></td>
			<td rowspan="<?=count($arResult["BASKET_ITEMS"])?>"><strong><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></strong></td>
			<?endif;?>
		</tr>
		<?
	}
	?>
</table>
<br /><br />
<b><?=GetMessage("SOA_TEMPL_SUM_ADIT_INFO")?></b><br /><br />

<table>
	<tr>
		<td width="50%" align="left" valign="top"><?=GetMessage("SOA_TEMPL_SUM_COMMENTS")?><br />
			<textarea rows="4" cols="40" name="ORDER_DESCRIPTION"><?=$arResult["USER_VALS"]["ORDER_DESCRIPTION"]?></textarea>
		</td>
	</tr>
</table>
<div class="basket_all_submit">
	<input type="submit" onClick="return submitFormClick();" value="Подтвердить и оформить заказ">
</div>
		
</div>
	
	<div class="step_order">
	<?foreach($arResult["PAY_SYSTEM"] as $arPaySystem):
		if ($arPaySystem["CHECKED"]!="Y") continue;
		if (strlen($arPaySystem["DESCRIPTION"])>0):
	?>
		<p>Информация об оплате:</p>
		<ul>
			<li><span><?=$arPaySystem["DESCRIPTION"]?></span></li>
		</ul>
	<?	endif;?>
	<?endforeach;?>
	</div>
</div>
