<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="step_text sostav">
<p>Состав заказа:</p>

<table>
	<tr>
		<th>Наименование</th>
		<th>Количество</th>
		<th>Цена</th>
		<th>За доставку</th>
		<th>Общая стоимость</th>
	</tr>
	<?
	foreach($arResult["BASKET_ITEMS"] as $keyBasket=>$arBasketItems)
	{
		?>
		<tr>
			<td><?=$arBasketItems["NAME"]?></td>
			<td><?=$arBasketItems["QUANTITY"]?></td>
			<td align="right"><?=$arBasketItems["PRICE_FORMATED"]?></td>
			<?if ($keyBasket < 1):?>
			<td rowspan="<?=count($arResult["BASKET_ITEMS"])?>"><?=$arResult["DELIVERY_PRICE_FORMATED"]?></td>
			<td rowspan="<?=count($arResult["BASKET_ITEMS"])?>"><strong><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></strong></td>
			<?endif;?>
		</tr>
		<?
	}
	?>
</table>
<div class="basket_all_submit">
	<input type="submit" onClick="return submitFormClick();" value="Подтвердить и оформить заказ">
</div>		
</div>
