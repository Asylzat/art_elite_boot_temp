<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["ORDER"]))
{
$APPLICATION->SetPageProperty("order", $arResult["ORDER"]["ACCOUNT_NUMBER"]);
?>
<?
global $USER;
if ($USER->IsAdmin())
{
	print("<pre>");
	print_r($arResult["VARIABLES"]);
	print("</pre>");
}
?>
	<div class="spasibo_head">  
	<p><span>Номер вашего заказа:</span> <?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?></p>
	<p><span>Стоимость заказа:</span> <?=number_format($arResult["ORDER"]["PRICE"], 0, '.', ' ')?> руб.</p>
</div> 
<div class="spasibo_sostav">  
	<p>Состав заказа:</p> 
	
	<table>
	<?foreach($arResult["ORDER"]["ITEMS"] as $arItem):?>
	<tr>
		<td><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></td><td><?=$arItem["QUANTITY"]?> шт</td><td><?=number_format($arItem["QUANTITY"]*$arItem["PRICE"], 0, '.', ' ')?> руб.</td>
	</tr>
	<?endforeach;?>
	</table>
	
	<p class="all"><span>Стоимость доставки:</span> <?=number_format($arResult["ORDER"]["PRICE_DELIVERY"], 0, '.', ' ')?> руб.</p>
	
</div>
<p>В ближайшее время с Вами свяжется наш менеджер для уточнения правильности заказа и времени доставки.</p>
<p class="spasibo_bold">Спасибо за покупку!</p>
	<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("includes/YaParams.php"), Array("orderId"=>$arResult["ORDER_ID"]), Array("MODE"=>"php") );?>
	<?
	if (!empty($arResult["PAY_SYSTEM"]))
	{
		?>
		<br /><br />
			<?
			if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0 && $arResult["ORDER"]["PAYED"] != "Y")
			{
				if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
						{
							?>
							<script language="JavaScript">
								window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
							</script>
							<?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))))?>
							<?
							if (CSalePdf::isPdfAvailable())
							{
								?><br />
								<?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y")) ?>
								<?
							}
						}
						else
						{
							if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
							{
								include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
							}
						}
						
			}
	}
}

else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ORDER_ID"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
?>
