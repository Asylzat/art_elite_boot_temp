<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$styleNextTab = ' style="display:none"';
$styleBasketStep = ' style="display:block"';
if ($_REQUEST["ACTIVE_STEP"] <> $step)
{
	$styleNextTab = ' style="display:block"';
	$styleBasketStep = ' style="display:none"';
}
?>
<div class="next_tab" id="tab_name_<?=$step?>"<?=$styleNextTab?>><span>ШАГ <?=$step?> <?=GetMessage("SOA_TEMPL_PAY_SYSTEM")?></span><a href="javascript:void(0)" class="link" onclick="clickStep(<?=$step?>)" <?if (intVal($_REQUEST["ACTIVE_STEP"])<$step):?>style="display:none"<?endif;?>>Развернуть</a></div>
<div class="basket_step selected_tab" id="tab_content_<?=$step?>"<?=$styleBasketStep?>>
	<div class="step_name"><span>ШАГ <?=$step++?></span></div>
	<div class="step_text">
		<p><?=GetMessage("SOA_TEMPL_PAY_SYSTEM")?><span class="error">*</span></p>
	</div>
	<div class="step_form">
	<?
	if ($arResult["PAY_FROM_ACCOUNT"]=="Y")
	{
		?>
		<tr>
		<td colspan="2">
		<input type="hidden" name="PAY_CURRENT_ACCOUNT" value="N">
		<input type="checkbox" name="PAY_CURRENT_ACCOUNT" id="PAY_CURRENT_ACCOUNT" value="Y"<?if($arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y") echo " checked=\"checked\"";?> onChange="submitForm()">
		<label for="PAY_CURRENT_ACCOUNT"><b><?=GetMessage("SOA_TEMPL_PAY_ACCOUNT")?></b></label><br />
		<?=GetMessage("SOA_TEMPL_PAY_ACCOUNT1")?> <b><?=$arResult["CURRENT_BUDGET_FORMATED"]?></b>
		<? if ($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y"): ?>
		. <?=GetMessage("SOA_TEMPL_PAY_ACCOUNT3")?>
		<? else: ?>
		, <?=GetMessage("SOA_TEMPL_PAY_ACCOUNT2")?>
		<? endif; ?>
		<br /><br />
		</td></tr>
		<?
	}
	$actualPaySystem = array();
	foreach($arResult["PAY_SYSTEM"] as $arPaySystem)
	{
		
		if(count($arResult["PAY_SYSTEM"]) == 1)
		{
			?>
			<tr>
			<td colspan="2">
			<input type="hidden" name="PAY_SYSTEM_ID" value="<?=$arPaySystem["ID"]?>">
			<b><?=$arPaySystem["NAME"];?></b>
			<?
			if (strlen($arPaySystem["DESCRIPTION"])>0)
			{
				?>
				<?=$arPaySystem["DESCRIPTION"]?>
				<br />
				<?
			}
			?>
			</td>
			</tr>
			<?
		}
		else
		{
			if ($arPaySystem["CHECKED"]=="Y") $actualPaySystem = $arPaySystem;
			//if (!isset($_POST['PAY_CURRENT_ACCOUNT']) OR $_POST['PAY_CURRENT_ACCOUNT'] == "N") {
			?>
			<div><span><label for="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"><?= $arPaySystem["PSA_NAME"] ?></label></span><input class="courier" type="radio" id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>" name="PAY_SYSTEM_ID" value="<?= $arPaySystem["ID"] ?>"<?if ($arPaySystem["CHECKED"]=="Y") echo " checked=\"checked\"";?> onclick="submitForm();" /></div>
			<?
			//}
		}
	}
	?>

<p><a href="javascript:void(0)" class="button" onclick="NextStep(<?=$step?>)">Перейти к шагу <?=$step?></a></p>	
	</div>
	<div class="step_order">
	<?if (strlen($actualPaySystem["DESCRIPTION"]) > 0):?>
		<p>Информация об оплате:</p>
		<ul>
		<?if (strlen($actualPaySystem["DESCRIPTION"])>0):?>
			<li><span><?=$actualPaySystem["DESCRIPTION"];?></span></li>
			<?endif;?>
		<?endif;?>
		</ul>
	</div>
</div>