<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$links = "";
$all = true;
asort($arResult["arrProp"][2]["VALUE_LIST"]);
foreach ($arResult["arrProp"][2]["VALUE_LIST"] as $keyValue => $value)
{	
	if (empty($value)) continue;
	$url = $APPLICATION->GetCurPageParam(urlencode($arResult["FILTER_NAME"] . '_pf[SIZE]') . '=' . $keyValue . "&set_filter=Y", array($arResult["FILTER_NAME"] . '_pf',"set_filter")); 
	if (($_GET[$arResult["FILTER_NAME"] . '_pf']['SIZE'] == $keyValue) && (intVal($_GET[$arResult["FILTER_NAME"] . '_pf']['SIZE']) > 0))
	{
		$links .= '<a class="selected" href="' . $url . '">' . $value . "</a>";
		$all = false;
	}
	else
		$links .= '<a href="' . $url . '">' . $value . "</a>";
}
if (count($arResult["arrProp"][2]["VALUE_LIST"]) > 0)
$links .= '<a ' . (($all)?'class="selected" ':'') . 'href="' . $APPLICATION->GetCurDir() . '">Все</a>';
$arResult["SIZE"] = $links;

?>