<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!doctype html>
<html lang="ru-RU">
<head>
<title><?$APPLICATION->ShowTitle()?></title>
<?$APPLICATION->ShowHead()?>
<meta name="author" content="ArtElite">
<meta name="apple-itunes-app" content="app-id=895042153">
<meta name="google-play-app" content="app-id=ru.artelite.app">
	<meta name="yandex-verification" content="9b00df92cbfbb53f" />


    <!-- Bootstrap core CSS -->
    <link href="<?=SITE_TEMPLATE_PATH?>/boot_files/bootstrap.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?=SITE_TEMPLATE_PATH?>/boot_files/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?=SITE_TEMPLATE_PATH?>/boot_files/starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?=SITE_TEMPLATE_PATH?>/boot_files/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?=SITE_TEMPLATE_PATH?>/boot_files/jquery.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="<?=SITE_TEMPLATE_PATH?>/boot_files/bootstrap.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?=SITE_TEMPLATE_PATH?>/boot_files/ie10-viewport-bug-workaround.js"></script>


<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.8.3.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui-1.9.2.custom.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.anythingslider.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.carouFredSel-6.0.4-packed.js"></script> 
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.formstyler.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.colorbox.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.inputmask.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox.js?v=2.1.5"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/slider.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.smartbanner.js"></script>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>
<script type="text/javascript">VK.init({apiId: 4483348, onlyWidgets: true});</script>







<script>
var inCompare = <?=count(array_keys($_SESSION["CATALOG_COMPARE_LIST"]["1"]["ITEMS"]));?>;
var maxCompare = 4;
$(document).ready(function(){ 
	checkCompare(<?=json_encode(array_keys($_SESSION["CATALOG_COMPARE_LIST"]["1"]["ITEMS"]))?>);
	$('.addToCart').each(function(){
		$(this).unbind("click");
		$(this).bind("click", function(){
			BX.ajax.post(
				$(this).attr('href'),
				"",
				function(){
					BX.ajax.post(
						"<?=SITE_TEMPLATE_PATH?>/includes/top_cart.php",
						"",
						function(data){
							$("#top_cart").html(data);
							$.colorbox({href: "<?=SITE_TEMPLATE_PATH?>/includes/AddToCart.php", width:"650px"});
						}
					);	
				}
			);
			return false;
		});
	});
	$('.addToCartOneClick').each(function(){
		$(this).unbind("click");
		$(this).bind("click", function(){
			BX.ajax.post(
				$(this).attr('href'),
				"",
				function(){
					window.location.href = "/personal/order/make/fast/";
				}
			);
			return false;
		});
	});
	$('.button_add').each(function(){
		$(this).unbind("click");
		$(this).bind("click", function(){
			$.fancybox({href:'<?=SITE_TEMPLATE_PATH?>/includes/'+$(this).data('address')+'.php', type:'ajax'});
			return false;
		});
	});
	$('.element_send a').each(function(){
		$(this).unbind("click");
		$(this).bind("click", function(){
			$.fancybox({href:'<?=SITE_TEMPLATE_PATH?>/includes/notice_form.php', type:'ajax', ajax:{data:{id:$(this).data('elementid')}}});
			return false;
		});
	});
});

function SendCompare(action, id)
{
	BX.ajax.post(
		"<?=SITE_TEMPLATE_PATH?>/includes/compare.php?action="+action+"&id="+id,
		"",
		function(data){
			$("#compare").html(data);
		}
	);
}
function AddToCompare(elem)
{
	var action = "DELETE_FROM_COMPARE_LIST";
	if ($(elem).attr("checked") == "checked")
	{
		action = "ADD_TO_COMPARE_LIST";
		if ((maxCompare - inCompare) < 1) 
		{
			$(elem).removeAttr("checked");
			return;
		}
		inCompare++;
	}
	else
	{
		inCompare--;	
	}
	
	SendCompare(action, $(elem).attr("id"));
}
</script>   



<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

</head> 

<body><?$APPLICATION->ShowPanel()?>
<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("includes/top_counter.php"), Array(), Array("MODE"=>"php") );?>
<div class="container header hidden-sm hidden-xs">
	<div class="logo hidden-md"><a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" width="302" height="89" alt="Цифровая живопись или наборы для раскрашивания по номерам (цифрам)" /></a></div>
	<div class="top_time">
		<p>Время работы магазина:<br />пн-пт: с 10:00 до 18:00</p>
		<p>Доставка по всей России</p>
		<p>Бесплатная доставка от 2 000р<br />по Москве и Санкт-Петербургу</p>
	</div>



	<div class="top_auth">
	<?$APPLICATION->IncludeComponent(
	"bitrix:system.auth.form",
	"top",
	Array(
	"REGISTER_URL" => "/personal/auth.php",
	"FORGOT_PASSWORD_URL" => "",
	"PROFILE_URL" => "/personal/",
	"SHOW_ERRORS" => "Y"
	),
	false
	);?>
		<p>Есть вопросы? Звоните нам!</p>
		<p><span class="ya-phone">8 (495) 641-67-49</span> <a class="callback fancybox.ajax" href="<?=SITE_TEMPLATE_PATH?>/includes/callback.php">Перезвоните мне</a></p>
	</div>	
	<div class="top_social"><?$APPLICATION->IncludeComponent("bitrix:catalog.section", "soc", array(
	"IBLOCK_TYPE" => "system",
	"IBLOCK_ID" => "9",
	"SECTION_ID" => $_REQUEST["SECTION_ID"],
	"SECTION_CODE" => "",
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "",
	),
	"ELEMENT_SORT_FIELD" => "sort",
	"ELEMENT_SORT_ORDER" => "asc",
	"ELEMENT_SORT_FIELD2" => "id",
	"ELEMENT_SORT_ORDER2" => "desc",
	"FILTER_NAME" => "arrFilter",
	"INCLUDE_SUBSECTIONS" => "Y",
	"SHOW_ALL_WO_SECTION" => "N",
	"HIDE_NOT_AVAILABLE" => "N",
	"PAGE_ELEMENT_COUNT" => "3",
	"LINE_ELEMENT_COUNT" => "1",
	"PROPERTY_CODE" => array(
		0 => "URL",
		1 => "",
	),
	"OFFERS_LIMIT" => "5",
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"ADD_SECTIONS_CHAIN" => "N",
	"DISPLAY_COMPARE" => "N",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"CACHE_FILTER" => "N",
	"PRICE_CODE" => array(
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRODUCT_PROPERTIES" => array(
	),
	"USE_PRODUCT_QUANTITY" => "N",
	"CONVERT_CURRENCY" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?></div>
	<div id="top_cart"><?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("includes/top_cart.php"), Array(), Array("MODE"=>"php") );?></div>
	<div class="clear"></div>
	<div id="quick_search">
	</div>
</div>






<script>
$(function(){
	//$('.navbar ul').addClass("");
	//$('.navbar ul').removeAttr("id");
	//$('.navbar ul ul').parent().addClass("dropdown");
	//$('.navbar ul ul').removeClass("nav navbar-nav").addClass("dropdown-menu");
});
</script>
<div class="container">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


		<?$APPLICATION->IncludeComponent("bitrix:menu", "top", array(
			"ROOT_MENU_TYPE" => "top",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => array(
			),
			"MAX_LEVEL" => "2",
			"CHILD_MENU_TYPE" => "podmenu",
			"USE_EXT" => "Y",	
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N"
			),
			false
		);?>
	<div class="navbar-form navbar-right"><?$APPLICATION->IncludeComponent("bitrix:search.form", "top", Array("PAGE" => "/search/index.php",),false);?></div>
    </div><!-- /.navbar-collapse -->



  </div><!-- /.container-fluid -->
</nav>
<div class="clear topmenu_fon"></div> 
</div>


	
	
<div class="container"> <!--id="container" -->


<div class="row">

	<!-- the main container row starts here (menu - content) -->


 
		<?/*if($APPLICATION->GetCurPage(true) == '/index.php'):?><?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("includes/main_slider.php"), Array(), Array("MODE"=>"html") );?><?endif*/?>
		 
		 
		<? if(!CSite::Indir('/contacts/') && !CSite::Indir('/personal/')): ?>


	<div class="col-md-3"> 
		
		<div id="left">
		<?if(!CSite::InDir("/catalog/compare.php")):?> 
		<div id="compare">
			<?$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("includes/compare.php"), Array(), Array("MODE"=>"html") );?>
		</div>
		<?endif?>




		<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "left_menu", array(
			"IBLOCK_TYPE" => "content",
			"IBLOCK_ID" => "1",
			"SECTION_ID" => $_REQUEST["SECTION_ID"],
			"SECTION_CODE" => "",
			"COUNT_ELEMENTS" => "Y",
			"TOP_DEPTH" => "1",
			"SECTION_FIELDS" => array(
				0 => "",
				1 => "",
			),
			"SECTION_USER_FIELDS" => array(
				0 => "UF_NOT_SHOW_IN_MENU",
				1 => "",
			),
			"SECTION_URL" => "/catalog/#SECTION_CODE#/",
			"CACHE_TYPE" => "N",
			"CACHE_TIME" => "36000000",
			"CACHE_GROUPS" => "Y",
			"ADD_SECTIONS_CHAIN" => "N"
			),
			false
		);?> 
		<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "left_menu_mozaiki", array(
			"IBLOCK_TYPE" => "content",
			"IBLOCK_ID" => "19",
			"SECTION_ID" => $_REQUEST["SECTION_ID"],
			"SECTION_CODE" => "",
			"COUNT_ELEMENTS" => "Y",
			"TOP_DEPTH" => "1",
			"SECTION_FIELDS" => array(
				0 => "",
				1 => "",
			),
			"SECTION_USER_FIELDS" => array(
				0 => "UF_NOT_SHOW_IN_MENU",
				1 => "",
			),
			"SECTION_URL" => "/almaznaya_vyshivka/#SECTION_CODE#/",
			"CACHE_TYPE" => "N",
			"CACHE_TIME" => "36000000",
			"CACHE_GROUPS" => "Y",
			"ADD_SECTIONS_CHAIN" => "N"
			),
			false
		);?>
		<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "left_menu_lenti", array(
			"IBLOCK_TYPE" => "content",
			"IBLOCK_ID" => "20",
			"SECTION_ID" => $_REQUEST["SECTION_ID"],
			"SECTION_CODE" => "",
			"COUNT_ELEMENTS" => "Y",
			"TOP_DEPTH" => "1",
			"SECTION_FIELDS" => array(
				0 => "",
				1 => "",
			),
			"SECTION_USER_FIELDS" => array(
				0 => "UF_NOT_SHOW_IN_MENU",
				1 => "",
			),
			"SECTION_URL" => "/vyshivka_lentami/#SECTION_CODE#/",
			"CACHE_TYPE" => "N",
			"CACHE_TIME" => "36000000",
			"CACHE_GROUPS" => "Y",
			"ADD_SECTIONS_CHAIN" => "N"
			),
			false
		);?>

		<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "video", array(
			"IBLOCK_TYPE" => "system",
			"IBLOCK_ID" => "10",
			"SECTION_ID" => $_REQUEST["SECTION_ID"],
			"SECTION_CODE" => "",
			"SECTION_USER_FIELDS" => array(
				0 => "",
				1 => "",
			),
			"ELEMENT_SORT_FIELD" => "sort",
			"ELEMENT_SORT_ORDER" => "asc",
			"ELEMENT_SORT_FIELD2" => "id",
			"ELEMENT_SORT_ORDER2" => "desc",
			"FILTER_NAME" => "arrFilter",
			"INCLUDE_SUBSECTIONS" => "Y",
			"SHOW_ALL_WO_SECTION" => "N",
			"HIDE_NOT_AVAILABLE" => "N",
			"PAGE_ELEMENT_COUNT" => "1",
			"LINE_ELEMENT_COUNT" => "1",
			"PROPERTY_CODE" => array(
				0 => "COD",
				1 => "",
			),
			"OFFERS_LIMIT" => "5",
			"SECTION_URL" => "",
			"DETAIL_URL" => "",
			"BASKET_URL" => "/personal/basket.php",
			"ACTION_VARIABLE" => "action",
			"PRODUCT_ID_VARIABLE" => "id",
			"PRODUCT_QUANTITY_VARIABLE" => "quantity",
			"PRODUCT_PROPS_VARIABLE" => "prop",
			"SECTION_ID_VARIABLE" => "SECTION_ID",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_GROUPS" => "Y",
			"META_KEYWORDS" => "-",
			"META_DESCRIPTION" => "-",
			"BROWSER_TITLE" => "-",
			"ADD_SECTIONS_CHAIN" => "N",
			"DISPLAY_COMPARE" => "N",
			"SET_TITLE" => "Y",
			"SET_STATUS_404" => "N",
			"CACHE_FILTER" => "N",
			"PRICE_CODE" => array(
			),
			"USE_PRICE_COUNT" => "N",
			"SHOW_PRICE_COUNT" => "1",
			"PRICE_VAT_INCLUDE" => "Y",
			"PRODUCT_PROPERTIES" => array(
			),
			"USE_PRODUCT_QUANTITY" => "N",
			"CONVERT_CURRENCY" => "N",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"PAGER_TITLE" => "Товары",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => "",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"AJAX_OPTION_ADDITIONAL" => ""
			),
			false
		);?>


		<?$APPLICATION->IncludeComponent(
			"bitrix:catalog.section",
			"news_left",
			Array(
				"AJAX_MODE" => "N",
				"IBLOCK_TYPE" => "content",
				"IBLOCK_ID" => "12",
				"SECTION_ID" => "",
				"SECTION_CODE" => "",
				"SECTION_USER_FIELDS" => array(),
				"ELEMENT_SORT_FIELD" => "DATE_ACTIVE_FROM",
				"ELEMENT_SORT_ORDER" => "desc",
				"ELEMENT_SORT_FIELD2" => "DATE_ACTIVE_FROM",
				"ELEMENT_SORT_ORDER2" => "desc",
				"FILTER_NAME" => "arrFilter",
				"INCLUDE_SUBSECTIONS" => "A",
				"SHOW_ALL_WO_SECTION" => "Y",
				"SECTION_URL" => "",
				"DETAIL_URL" => "",
				"BASKET_URL" => "/personal/basket.php",
				"ACTION_VARIABLE" => "action",
				"PRODUCT_ID_VARIABLE" => "id",
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"SECTION_ID_VARIABLE" => "SECTION_ID",
				"META_KEYWORDS" => "-",
				"META_DESCRIPTION" => "-",
				"BROWSER_TITLE" => "-",
				"ADD_SECTIONS_CHAIN" => "N",
				"DISPLAY_COMPARE" => "N",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"PAGE_ELEMENT_COUNT" => "3",
				"LINE_ELEMENT_COUNT" => "0",
				"PROPERTY_CODE" => array(),
				"OFFERS_LIMIT" => "5",
				"PRICE_CODE" => array(),
				"USE_PRICE_COUNT" => "N",
				"SHOW_PRICE_COUNT" => "1",
				"PRICE_VAT_INCLUDE" => "Y",
				"PRODUCT_PROPERTIES" => array(),
				"USE_PRODUCT_QUANTITY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Товары",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"HIDE_NOT_AVAILABLE" => "N",
				"CONVERT_CURRENCY" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N"
			),
		false
		);?>

		<?$APPLICATION->IncludeComponent("bitrix:menu", "accessories", array(
			"ROOT_MENU_TYPE" => "accessories",
			"MENU_CACHE_TYPE" => "A",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => array(
			),
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "left",
			"USE_EXT" => "Y",
			"ALLOW_MULTI_SELECT" => "N",
			"ONLY_IBLOCK" => "Y"
			),
			false
		);?>

			
		<?$APPLICATION->IncludeComponent("bitrix:menu", "producers", array(
			"ROOT_MENU_TYPE" => "producers",
			"MENU_CACHE_TYPE" => "A",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => array(
			),
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "left",
			"USE_EXT" => "Y",
			"ALLOW_MULTI_SELECT" => "N",
			"ONLY_IBLOCK" => "Y"
			),
			false
		);?>


		<?$APPLICATION->IncludeComponent("bitrix:menu", "type", array(
			"ROOT_MENU_TYPE" => "type",
			"MENU_CACHE_TYPE" => "A",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => array(
			),
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "type",
			"USE_EXT" => "Y",
			"ALLOW_MULTI_SELECT" => "N",
			"ONLY_IBLOCK" => "Y"
			),
			false
		);?>



		<?if($APPLICATION->GetCurPage(true) != '/index.php'):?>

		<?
		$arUrl = explode("/", trim($APPLICATION->GetCurPage(false), "/"));
		if((count($arUrl) < 3))
		{
			$APPLICATION->IncludeFile($APPLICATION->GetTemplatePath("includes/left_sale.php"), Array(), Array("MODE"=>"html") );
		}
		?>

		<?endif?>


		   
		</div>


	</div>



	<div class="col-md-9">


		<div id="center">
		 
		<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","top",Array("START_FROM" => "0", "PATH" => "", "SITE_ID" => "-"), false);?>
		 
		<?
		$showFilter = false;
		$dir = trim($APPLICATION->GetCurDir(),"/");
		$arDir = explode("/", $dir);
		$sectObj = CIBlockSection::GetList(
		    array("SORT"=>"ASC"),
		    array("IBLOCK_ID" => array(1,19), "CODE" => $arDir[count($arDir) - 1]),
		    false,
		    array(),
		    array("nPageSize" => 1)
		);
		if($sect = $sectObj->GetNext())
			$showFilter = true;
		//if (CSite::InDir("/catalog/") && (count($arDir) < 3) && !CSite::InDir("/catalog/compare.php")):
		if ((CSite::InDir("/catalog/") && $showFilter) || CSite::InDir("/catalog/index.php") || (CSite::InDir("/almaznaya_vyshivka/") && $showFilter) || CSite::InDir("/almaznaya_vyshivka/index.php")):

		?>




		<div id="top_filtr">
		<?$APPLICATION->IncludeComponent(
			"bitrix:catalog.filter", 
			"painting", 
			array(
				"IBLOCK_TYPE" => "content",
				"IBLOCK_ID" => (CSite::InDir("/catalog/") || CSite::InDir("/catalog/index.php"))?"1":"19",
				"FILTER_NAME" => "arrFilter",
				"FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"PROPERTY_CODE" => array(
					0 => "PRODUCER",
					1 => "SIZE2",
					2 => "",
				),
				"LIST_HEIGHT" => "5",
				"TEXT_WIDTH" => "20",
				"NUMBER_WIDTH" => "5",
				"CACHE_TYPE" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_GROUPS" => "Y",
				"SAVE_IN_SESSION" => "N",
				"PRICE_CODE" => array(
				)
			),
			false
		);?>
		</div>
		<?endif?>

		<?endif?>


		

		<? if(CSite::Indir('/personal/')): ?>
		<div class="one_page">

		<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","top",Array("START_FROM" => "0", "PATH" => "", "SITE_ID" => "-"), false);?>

		<?endif?>



