<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$result = array("status" => 0);
if(!empty($_POST["noticeId"]) && !empty($_POST["email"]))
{
	$notice = new Notice();
	global $DB;
	$noticeElement = array(
		'UF_EMAIL' => $_POST["email"],
		'UF_GOODS' => $_POST["noticeId"],
		'UF_DATETIME_REG' => date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time())
		);
	if($notice->AddNotice($noticeElement))
	{
		$result["status"] = 1;
		$_SESSION["NOTICE"][$_POST["noticeId"]] = $_POST["noticeId"];
	}
	
}
echo json_encode($result);
?>