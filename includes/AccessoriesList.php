<?
$breakCount = 4;
$count = 0;
CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
$sectionsObj = CIBlockSection::GetList(
	array("SORT"=>"ASC"),
	array(
		"ACTIVE" => "Y",
		//"DEPTH_LEVEL" => "1",
		"IBLOCK_ID" => 7,
		"!ID" => 34
	)
);
while ($section = $sectionsObj->GetNext())
{
	if ($count > $breakCount) break;
	$count++;
	$elementsObj = CIBlockElement::GetList(
		array("rand"=>"ASC"),
		array(
			"SECTION_ID" => $section["ID"],
			"INCLUDE_SUBSECTIONS" => "Y",
			"IBLOCK_ID" => $section["IBLOCK_ID"]
		)
	);
	while ($elementObj = $elementsObj->GetNextElement())
	{
		$element = $elementObj->GetFields();
		$elementCatalog = CCatalogProduct::GetByID($element["ID"]);
		$resPriceObj = CPrice::GetList(
			array(),
			array("PRODUCT_ID" => $elementCatalog["ID"])
		);
		$element["CAN_BUY"] = "N";
		if ($resPrice = $resPriceObj->GetNext())
		{
			$element["CAN_BUY"] = $resPrice["CAN_BUY"];
			$element["CURRENCY"] = $resPrice["CURRENCY"];
			$element["PRICE"] = $resPrice["PRICE"];
			$element["ADD_URL"] = CMain::GetCurPage(false) . '?action=ADD2BASKET&id=' . $element["ID"];
		}
		if ($elementCatalog["QUANTITY"] > 0)
		{
			$el = $element;
			$el["PROPERTIES"] = $elementObj->GetProperties();
			$el["DISPLAY_PROPERTIES"][] = $el["PROPERTIES"]["ARTICUL"];
			if (intVal($el["PROPERTIES"]["PRODUCER"]["VALUE"] > 0))
			{
				$producer = CIBlockElement::GetById(intVal($el["PROPERTIES"]["PRODUCER"]["VALUE"]))->GetNext();
				$el["PROPERTIES"]["PRODUCER"]["VALUE"] = $producer["NAME"];
				$el["DISPLAY_PROPERTIES"][] = $el["PROPERTIES"]["PRODUCER"];
			}
			$arResult["ITEMS"][] = $el;
			break;
		}
	}
}
/*
for ($i = $count; $i < $breakCount; $i++)
{
	$elements[] = array();
}
*/
?>
<?
$rows = array();
$rowElements = array();
$count = 0;
foreach($arResult["ITEMS"] as $cell=>$arElement)
{
	$count++;
	$producer = CIBlockElement::GetByID($arElement["PROPERTIES"]["PRODUCER"]["VALUE"])->GetNext();
	$arResult["ITEMS"][$cell]["PROPERTIES"]["PRODUCER"]["VALUE"] = $producer["NAME"];
	if (intVal($arElement["PREVIEW_PICTURE"]) > 0)
	{
		$arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = ImgFileResizeFactory::ResizeImgById($arElement["PREVIEW_PICTURE"], 70, 70, "", ImgFileResizeFactory::CENTER_CROP, true);
	}
	else
	{
		foreach($arElement["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $photo)
		{
			$arResult["ITEMS"][$cell]["PREVIEW_PICTURE"] = ImgFileResizeFactory::ResizeImgById($photo, 70, 70, "", ImgFileResizeFactory::CENTER_CROP, true);
			break;
		}
	}
	if ($count == $breakCount)
	{
		$count = 0;
		$rowElements[] = $arResult["ITEMS"][$cell];
		$rows[] = $rowElements;
		$rowElements = array();
	}
	else
	{
		$rowElements[] = $arResult["ITEMS"][$cell];
	}	
}
if ($count > 0)
{
	for($i = $count; $i<$breakCount; $i++)
		$rowElements[] = array();
	$rows[] = $rowElements;
}
$arResult["ROWS"] = $rows;

?>
<?if(count($arResult["ROWS"])>0):?>
<div class="element_need">
<p class="head">Дополнительные аксессуары</p> 
<?foreach($arResult["ROWS"] as $cell=>$rowElements):?> 
<?foreach($rowElements as $key=>$arElement):?> 
	<div><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?if($arElement["PREVIEW_PICTURE"]["SRC"]):?><img src="<?print_r($arElement["PREVIEW_PICTURE"]["SRC"]);?>" width="<?print_r($arElement["PREVIEW_PICTURE"]["WIDTH"]);?>" height="<?print_r($arElement["PREVIEW_PICTURE"]["HEIGHT"]);?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /><?endif?> <?=$arElement["NAME"]?></a> <?echo '<span class="catalog-price">'.FormatCurrency($arElement["PRICE"], $arElement["CURRENCY"])."</span>";?></div> 
<?	endforeach;?> 
<?endforeach;?> 
</div>
<?endif;?>

