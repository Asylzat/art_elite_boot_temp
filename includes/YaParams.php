<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
$dbBasketItems = CSaleBasket::GetList(
	array(
			"PRICE" => "ASC"
		),
	array(
			"ORDER_ID" => $orderId
		),
	false,
	false,
	array(
		"NAME", "PRODUCT_ID", "PRICE", "CURRENCY", "WEIGHT", "QUANTITY", "LID", "DELAY", "CAN_BUY", "NAME", "CALLBACK_FUNC", "MODULE", "NOTES", "ORDER_CALLBACK_FUNC", "DETAIL_PAGE_URL", "CANCEL_CALLBACK_FUNC", "PAY_CALLBACK_FUNC", "FUSER_ID"
	)
);
$quantity = 0;
$price = 0;
$elements = array();
while ($arItem = $dbBasketItems->Fetch())
{
	$quantity += $arItem["QUANTITY"];
	$price += $arItem["PRICE"]*$arItem["QUANTITY"];
	$elements[] = $arItem;
}

$text = '<script type="text/javascript">
	yaParams = 
	{
		order_id: "' . $orderId . '",
		order_price: ' . $price . ',
		currency: "RUR", 
		exchange_rate: 1,
		goods: 
			[
				';
$zpt = '';				
foreach($elements as $key => $element)
{
if ($key > 0) $zpt = ',
				';
$text .= $zpt . '{
					id: "' . $element["PRODUCT_ID"] . '",
					name: "' . $element["NAME"] . '",
					price: ' . $element["PRICE"] . ',
					quantity: ' . intVal($element["QUANTITY"]) . '
				}';
}
$text .= '
			]
	};
</script>';
echo $text;
?>
