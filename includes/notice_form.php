<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<form id="notice_form">
<p>Введите ваш email и мы уведомим Вас о поступлении товара в продажу</p>
<p><input class="bx-auth-input" name="email" type="text" value=""/></p>
<input name="noticeId" type="hidden" value="<?=$_REQUEST["id"]?>"/>
<input id="notice_send" type="button" class="button" value="Уведомить"/>
</form>
<div id="success_notice" style="display:none">Как только товар поступит в продажу вы будете уведомлены</div>
<script>
$('#notice_send').bind('click', function(){
	BX.ajax.post(
		"<?=SITE_TEMPLATE_PATH?>/includes/notice_add.php",
		$('#notice_form').serialize(),
		function(data){
			 data = jQuery.parseJSON(data);
			 if (data.status == 1)
			 {
				$("#notice_form").hide();
				$("#success_notice").show();
			 }
		}
	);
	return false;
})
</script>
