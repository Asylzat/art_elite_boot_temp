<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if ((count($_SESSION["CATALOG_COMPARE_LIST"]["1"]["ITEMS"]) > 3) && ($_REQUEST["action"] != "DELETE_FROM_COMPARE_LIST"))
{
	$_REQUEST["action"] = "";
	$_REQUEST["id"] = "";
}
$APPLICATION->IncludeComponent(
	"bitrix:catalog.compare.list",
	".default",
	Array(
		"AJAX_MODE" => "Y",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => 1,
		"DETAIL_URL" => "",
		"COMPARE_URL" => "/catalog/compare.php",
		"NAME" => "CATALOG_COMPARE_LIST",
		"AJAX_OPTION_SHADOW" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
false
);?>