<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION;
global $USER;
if ($_REQUEST["login"] == "yes"):
?>
<div class="step_name"><span>ШАГ 1</span></div>
	<div class="step_text">
		<p>Введите свои данные <span class="error">*</span></p>
		<p><em>Введите логин/email и пароль, и введенные вами ранее данные автоматически заполнятся</em></p>
	</div>
	<div class="step_form">	
<?$APPLICATION->IncludeComponent(
	"bitrix:system.auth.form",
	"",
	Array(
	"REGISTER_URL" => "/personal/auth.php",
	"FORGOT_PASSWORD_URL" => "",
	"PROFILE_URL" => "/personal/",
	"SHOW_ERRORS" => "Y"
	),
	false
	);?>
	<p><a href="" class="button">Перейти к шагу 2</a></p>	
</div>
<?endif;?>	
