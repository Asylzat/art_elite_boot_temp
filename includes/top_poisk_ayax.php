<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
global $APPLICATION;
$APPLICATION->IncludeComponent("bitrix:search.page", "quick_search", Array(
	"RESTART" => "Y",	// Искать без учета морфологии (при отсутствии результата поиска)
	"NO_WORD_LOGIC" => "N",	// Отключить обработку слов как логических операторов
	"CHECK_DATES" => "N",	// Искать только в активных по дате документах
	"USE_TITLE_RANK" => "N",	// При ранжировании результата учитывать заголовки
	"DEFAULT_SORT" => "rank",	// Сортировка по умолчанию
	"FILTER_NAME" => "ARTICLE",	// Дополнительный фильтр
	"arrFILTER" => array("iblock_content"),
	"arrFILTER_iblock_content" => array("1"),
	"SHOW_WHERE" => "N",	// Показывать выпадающий список "Где искать"
	"SHOW_WHEN" => "N",	// Показывать фильтр по датам
	"PAGE_RESULT_COUNT" => "30",	// Количество результатов на странице
	"AJAX_MODE" => "N",	// Включить режим AJAX
	"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
	"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
	"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
	"CACHE_TYPE" => "A",	// Тип кеширования
	"CACHE_TIME" => "3600",	// Время кеширования (сек.)
	"DISPLAY_TOP_PAGER" => "N",	// Выводить над результатами
	"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под результатами
	"PAGER_TITLE" => "Результаты поиска",	// Название результатов поиска
	"PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
	"PAGER_TEMPLATE" => "forum",	// Название шаблона
	"USE_LANGUAGE_GUESS" => "N",	// Включить автоопределение раскладки клавиатуры
	"USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
	"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
	),
	false
);?>