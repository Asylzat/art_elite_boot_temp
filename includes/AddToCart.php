<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");
$dbBasketItems = CSaleBasket::GetList(
	array(
			"DATE_INSERT" => "DESC"
		),
	array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
	false,
	false,
	array(
		"NAME", "PRODUCT_ID", "PRICE", "CURRENCY", "WEIGHT", "QUANTITY", "LID", "DELAY", "CAN_BUY", "NAME", "CALLBACK_FUNC", "MODULE", "NOTES", "ORDER_CALLBACK_FUNC", "DETAIL_PAGE_URL", "CANCEL_CALLBACK_FUNC", "PAY_CALLBACK_FUNC", "FUSER_ID"
	)
);
$quantity = 0;
$price = 0;
$element = array();
$first = true;
while ($arItem = $dbBasketItems->Fetch())
{
	$quantity += $arItem["QUANTITY"];
	$price += $arItem["PRICE"]*$arItem["QUANTITY"];
	if ($first) 
	{
		$element = $arItem;
		$first = false;
	}
}
?>

<div id="inline_content">		
<p class="button_head">Вы добавили товар “<a href="<?=$element["DETAIL_PAGE_URL"]?>"><?=$element["NAME"]?></a>” в корзину</p>
<p>В Вашей корзине сейчас товаров <strong>(<?=$quantity?>)</strong> на сумму <strong><?=$price?> руб.</strong></p>
<p>Вы можете сразу перейти в <a href="/personal/cart/">корзину</a> заказа или <a href="javascript:void(0)" onclick="$.colorbox.close()">продолжить</a> покупки</p>
<span><a class="button_on" href="javascript:void(0)" onclick="$.colorbox.close()">&larr; Продолжить покупки</a><a class="button_off" href="/personal/cart/">Оформить заказ &rarr;</a></span>
<div class="clear"></div>
</div>